﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    /*public Slider jauge;

    public void SetHealth(int val)
    {
        jauge.value = val;
    }

    public void SetMaxHealth(int max)
    {
        jauge.maxValue = max;
        SetHealth(max);
    }*/

    private Vector3 _localScale;
    private Vector3 _startScale;
    private int _maxHealth;

    private void Awake()
    {
        _localScale = transform.localScale;
        _startScale = _localScale;
    }

    public void SetHealth(int hp)
    {
        if (_maxHealth != 0)
        {
            if (hp <= 0)
            {
                transform.localScale = new Vector3();
            }
            else
            {
                _localScale.x = _startScale.x / _maxHealth * hp;
                transform.localScale = _localScale;
            }
        }
    }
    
    public void SetMaxHealth(int max)
    {
        _maxHealth = max;
        SetHealth(max);
    }
    
}
