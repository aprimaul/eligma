﻿using System.Collections;
using System.Collections.Generic;
using Common;
using GameEntity;
using UnityEngine;

namespace Tools
{
    public delegate float EasingDelegate(float start, float end, float value);

    public delegate CustomVector3 MoveChoiceBehaviorDelegate(FreeDirectionEvent e);

    public delegate Spell SpellChoiceBehaviorDelegate();
}