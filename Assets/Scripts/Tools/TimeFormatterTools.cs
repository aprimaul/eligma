﻿namespace Tools
{
    public class TimeFormatterTools
    {
        public static string FromSecondsToTime(float s)
        {
            int minutes = (int) s / 60;
            int secondes = (int) s%60;
            return minutes + "min" + secondes + "s";
        }
    }
}