﻿using System.Collections.Generic;
using UnityEngine;

namespace Tools
{
    public static class ComponentsSearchTools
    {
        public static T[] FindComponentsInChildWithTag<T>(this GameObject parent, string tag) where T : Component
        {
            if ((parent == null) || string.IsNullOrEmpty(tag)) return new T[0];
            List<T> list = new List<T>(parent.GetComponentsInChildren<T>());
            if(list.Count == 0) { return null; }
 
            for(int i = list.Count - 1; i >= 0; i--) 
            {
                if (list[i].CompareTag(tag) == false)
                {
                    list.RemoveAt(i);
                }
            }
            return list.ToArray();
        }
    }
}