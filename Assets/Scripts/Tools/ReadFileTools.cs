﻿using System.IO;
using System.Text;
using UnityEngine;

namespace Tools
{
    public class ReadFileTools
    {
        public static string ReadFile(string path)
        {
            TextAsset textfile = Resources.Load<TextAsset>(path);
            return textfile.text;
        }
    }
}