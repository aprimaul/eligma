using System.Collections.Generic;
using System.Linq;
using Common;
using UnityEngine;

namespace Tools
{
    public class DirectionTools : Singleton<DirectionTools>
    {
        public static Vector3Int[] AllDirectionsAI =
        {
            new Vector3Int(0, 1, 0),
            new Vector3Int(0, -1, 0),
            new Vector3Int(-1, 0, 0),
            new Vector3Int(1, 0, 0),
            new Vector3Int(-1, 1, 0),
            new Vector3Int(-1, -1, 0),
            new Vector3Int(1, -1, 0),
            new Vector3Int(1, 1, 0)
        };
        
        public static Vector3Int[] AllDirections =
        {
            new Vector3Int(0, 1, 0),
            new Vector3Int(0, -1, 0),
            new Vector3Int(-1, 0, 0),
            new Vector3Int(1, 0, 0),
            new Vector3Int(-1, 1, 0),
            new Vector3Int(-1, -1, 0),
            new Vector3Int(1, -1, 0),
            new Vector3Int(1, 1, 0),
            new Vector3Int(3, 1, 1),
            new Vector3Int(1, 3, 1),
            new Vector3Int(-3, -1, -1),
            new Vector3Int(-1, -3, -1)
        };

        private static Dictionary<Direction, Vector3> rotationIndex = new Dictionary<Direction, Vector3>()
        {
            {Direction.NorthWest,new Vector3(0,0,0)},
            {Direction.North, new Vector3(0,45,0)},
            {Direction.NorthEast,new Vector3(0,90,0)},
            {Direction.East,new Vector3(0,135,0)},
            {Direction.SouthEast,new Vector3(0,180,0)},
            {Direction.South,new Vector3(0,225,0)},
            {Direction.SouthWest,new Vector3(0,270,0)},
            {Direction.West,new Vector3(0,315,0)}
        };
        
        private static Dictionary<Direction, Vector3Int> rotationMatrixIndex = new Dictionary<Direction, Vector3Int>()
        {
            {Direction.North, new Vector3Int(1,1,0)},
            {Direction.East, new Vector3Int(-1,1,0)},
            {Direction.South, new Vector3Int(-1,-1,0)},
            {Direction.West, new Vector3Int(1,-1,0)}
        };
        
        static Dictionary<Vector3Int, Direction> revertedDirectionIndex = new Dictionary<Vector3Int, Direction>()
        {
            {AllDirections[0], Direction.North},
            {AllDirections[1], Direction.South},
            {AllDirections[2], Direction.East},
            {AllDirections[3], Direction.West},
            {AllDirections[4], Direction.NorthEast},
            {AllDirections[5], Direction.SouthEast},
            {AllDirections[6], Direction.SouthWest},
            {AllDirections[7], Direction.NorthWest},
            {AllDirections[8], Direction.West},
            {AllDirections[9], Direction.North},
            {AllDirections[10], Direction.East},
            {AllDirections[11], Direction.South}
        };

        private static Dictionary<Direction, Vector3Int> directionIndex = CreateIndex();

        private static Dictionary<Direction, Vector3Int> CreateIndex()
        {
            Dictionary<Direction, Vector3Int> index = new Dictionary<Direction, Vector3Int>();

            foreach (var direction in 
                revertedDirectionIndex.Where(direction => !index.ContainsKey(direction.Value)))
            {
                index.Add(direction.Value, direction.Key);
            }

            return index;
        }

        public static Direction GetStaticDirectionFrom(Direction d)
        {
            if (d.Equals(Direction.NorthWest) || d.Equals(Direction.North))
            {
                return Direction.North;
            }
            if (d.Equals(Direction.NorthEast) || d.Equals(Direction.East))
            {
                return Direction.East;
            }

            if (d.Equals(Direction.SouthWest) || d.Equals(Direction.West))
            {
                return Direction.West;
            }

            return Direction.South;
        }

        public static Direction Vector3IntToDirection(Vector3Int v)
        {
            return !revertedDirectionIndex.ContainsKey(v) ? Direction.North : revertedDirectionIndex[v];
        }

        public static Vector3Int DirectionToRotationVector(Direction d)
        {
            return rotationMatrixIndex.ContainsKey(d) ? rotationMatrixIndex[d] : rotationMatrixIndex[Direction.North];
        }
        
        public static Vector3 GetRotationFromDirection(Direction d)
        {
            return rotationIndex[d];
        }
    }
}