using System.Collections;
using System.Collections.Generic;
using Common;
using GameEntity;
using SDD.Events;
using Tools;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BoardManager : Manager<BoardManager>
{
    [SerializeField] private GameObject m_Tilemap;
    [SerializeField] private GameObject m_EntityList;

    private Entity[] m_Entities;

    private int nbEnnemy = 0;

    public MapMatrix BoardMatrix { private set; get; }

    public override void SubscribeEvents()
    {
        base.SubscribeEvents();
        EventManager.Instance.AddListener<AskFreeDirectionsEvent>(CheckFreeDirections);
        EventManager.Instance.AddListener<OnMoveEntityEvent>(MoveEntity);
        EventManager.Instance.AddListener<EntityDeathEvent>(RemoveEntity);
    }

    public override void UnsubscribeEvents()
    {
        base.UnsubscribeEvents();
        EventManager.Instance.RemoveListener<AskFreeDirectionsEvent>(CheckFreeDirections);
        EventManager.Instance.RemoveListener<OnMoveEntityEvent>(MoveEntity);
        EventManager.Instance.RemoveListener<EntityDeathEvent>(RemoveEntity);
    }

    protected override IEnumerator InitCoroutine()
    {
        Tilemap[] grids = m_Tilemap.GetComponentsInChildren<Tilemap>();
        BoardMatrix = new MapMatrix(grids);
        m_Entities = m_EntityList.GetComponentsInChildren<Entity>();

        foreach (var entity in m_Entities)
        {
            while (!entity.m_IsReady)
                yield return new WaitForSeconds(0.1f);
                
            if (entity.m_Team.Equals(Team.Red))
            {
                nbEnnemy++;
            }
                
            CustomVector3 customGeometric = CustomVector3.FromGeometricVector2(entity.GetGeometricPosition());
            Vector3Int position = customGeometric.ToMatrixVector3();
                
            entity.MatrixPosition = position;
            if (!entity.m_IsInvisible)
            {
                BoardMatrix.AddEntity(entity.MatrixPosition);
            }
        }
            
            
        BoardMatrix.DisplayStage(0, "EMP");

        yield return null;
    }

    private void MoveEntity(OnMoveEntityEvent e)
    {
        BoardMatrix.MoveEntity(e.eFrom, e.eTo);
    }

    private void CheckFreeDirections(AskFreeDirectionsEvent e)
    {
        List<CustomVector3> directionsGeometriques = new List<CustomVector3>();

        foreach (Vector3Int direction in e.eDirections)
        {
            Vector3Int stairsDirection = new Vector3Int(direction.x*3+direction.y,direction.x+3*direction.y, 1);
            Vector3Int v = e.eEntityPlayMode.MatrixPosition + new Vector3Int(direction.x, direction.y, direction.z);
            if(BoardMatrix.IsFree(v))
                directionsGeometriques.Add(CustomVector3.FromMapVector3(direction));

            else if(BoardMatrix.IsStairs(v) && (direction.Equals(new Vector3Int(1,0,0)) || direction.Equals(new Vector3Int(0,1,0))) && v.z == 0)
                directionsGeometriques.Add(CustomVector3.FromMapVector3(stairsDirection));
                
            else if(BoardMatrix.IsStairs(v) && (direction.Equals(new Vector3Int(-1,0,0)) || direction.Equals(new Vector3Int(0,-1,0))) && v.z == 1)
                directionsGeometriques.Add(CustomVector3.FromMapVector3(new Vector3Int(stairsDirection.x, stairsDirection.y, -1)));
        }

        if (directionsGeometriques.Count > 0)
            EventManager.Instance.Raise(new FreeDirectionEvent()
                {EntityPlayMode = e.eEntityPlayMode, directions = directionsGeometriques.ToArray()});
    }
        
    private void RemoveEntity(EntityDeathEvent e)
    {
        nbEnnemy--;
        if (nbEnnemy != 0) return;
            
        StartCoroutine(WaitEndGameCoroutine(HudManager.Instance.playerLife <= 0));

        if (HudManager.Instance.playerLife <= 0)
        {
            EventManager.Instance.Raise(new GameOverEvent());
        }
        else
        {
            Debug.Log("Victoire detectee par le boardManager");
            EventManager.Instance.Raise(new GameVictoryEvent());
        }
    }
        
    #region Coroutines
             
    private IEnumerator WaitEndGameCoroutine(bool gameover){
        yield return new WaitForSeconds(3);
        if(gameover)
            EventManager.Instance.Raise(new GameOverEvent());
        else
            EventManager.Instance.Raise(new GameVictoryEvent());
    }
             
    #endregion
}