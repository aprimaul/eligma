﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Buttons
{
    //TODO: Mieux héritage ou 2 scripts sur un composant
    public class PlayerGameButtonBehavior : GenericButtonBehavior
    {
        [FormerlySerializedAs("Id")] [SerializeField] public int m_Id;

        protected override void Start()
        {
            base.Start();
            GetComponent<Button>().GetComponentInChildren<Text>().text = 
                PlayerPrefs.HasKey(m_Id + ".NAME") ? PlayerPrefs.GetString(m_Id + ".NAME") : "New game";
        }
    }
}