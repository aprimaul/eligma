﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common;
using SDD.Events;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : Manager<LevelManager>
{
    public string IdLevel { get; private set; }
    private Scene sceneLevel;
    #region Manager implementation

    protected override IEnumerator InitCoroutine()
    {
        yield break;
    }

    #endregion

    public override void SubscribeEvents()
    {
        EventManager.Instance.AddListener<PlayButtonClickedEvent>(GameWorldPlay);
        EventManager.Instance.AddListener<WorldSelectionButtonClickedEvent>(GameWorldMenu);
        EventManager.Instance.AddListener<TryAgainGameEvent>(RestartLevel);
        base.SubscribeEvents();
    }

    public override void UnsubscribeEvents()
    {
        EventManager.Instance.RemoveListener<PlayButtonClickedEvent>(GameWorldPlay);
        EventManager.Instance.RemoveListener<TryAgainGameEvent>(RestartLevel);

        base.UnsubscribeEvents();
    }

    private void UnloadScene(string id)
    {
        if(id.Equals(Constants.FIRE)) SceneManager.UnloadSceneAsync(Constants.FIRESCENE);
        if (id.Equals(Constants.AIR)) SceneManager.UnloadSceneAsync(Constants.AIRSCENE);
        if(id.Equals(Constants.GROUND)) SceneManager.UnloadSceneAsync(Constants.GROUNDSCENE);
        if(id.Equals(Constants.WATER)) SceneManager.UnloadSceneAsync(Constants.WATERSCENE);
    }

    private void LoadScene(string id)
    {
        if (id.Equals(Constants.FIRE))
        {
            SceneManager.LoadScene(Constants.FIRESCENE, LoadSceneMode.Additive);
            if (MusicLoopsManager.Instance) MusicLoopsManager.Instance.PlayMusic(Constants.FIREWORLD_MUSIC);
        }

        if (id.Equals(Constants.AIR))
        {
            SceneManager.LoadScene(Constants.AIRSCENE, LoadSceneMode.Additive);
            if (MusicLoopsManager.Instance) MusicLoopsManager.Instance.PlayMusic(Constants.AIRWORLD_MUSIC);
        }

        if (id.Equals(Constants.GROUND))
        {
            SceneManager.LoadScene(Constants.GROUNDSCENE, LoadSceneMode.Additive);
            if (MusicLoopsManager.Instance) MusicLoopsManager.Instance.PlayMusic(Constants.GROUNDWORLD_MUSIC);
        }

        if (id.Equals(Constants.WATER))
        {
            SceneManager.LoadScene(Constants.WATERSCENE, LoadSceneMode.Additive);
            if (MusicLoopsManager.Instance) MusicLoopsManager.Instance.PlayMusic(Constants.WATERWORLD_MUSIC);
        }

        sceneLevel = SceneManager.GetActiveScene();
    }

    protected void RestartLevel(TryAgainGameEvent e)
    {
        UnloadScene(IdLevel);
        LoadScene(IdLevel);
    }
        
    protected void GameWorldMenu(WorldSelectionButtonClickedEvent e)
    {
        UnloadScene(IdLevel);
        SceneManager.UnloadSceneAsync("Hud");
        if (MusicLoopsManager.Instance) MusicLoopsManager.Instance.PlayMusic(Constants.MENU_MUSIC);
    }
        
    protected void GameWorldPlay(PlayButtonClickedEvent e)
    {
        IdLevel = e.tag.ToUpper();
        LoadScene(IdLevel);
        SceneManager.LoadScene("Hud", LoadSceneMode.Additive);
    }
    protected override void GamePlay(GamePlayEvent e)
    {
    }

    protected override void GameMenu(GameMenuEvent e)
    {
    }
}