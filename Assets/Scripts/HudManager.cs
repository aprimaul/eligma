﻿using System;
using System.Collections;
using System.Globalization;
using System.Threading;
using Common;
using SDD.Events;
using Tools;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class HudManager : Manager<HudManager>
{
    #region Labels & Values
    [SerializeField] private GameObject m_panelHitSpells;
    [SerializeField] private GameObject m_panelHealSpells;
    [SerializeField] private GameObject m_playerJauge;
    [FormerlySerializedAs("m_poisonJauge")] [SerializeField] private GameObject m_currentSpellJauge;
    [SerializeField] private GameObject m_panelBestScore;
    [SerializeField] private GameObject m_panelNameLevel;
    [SerializeField] private GameObject m_panelTimer;
        
    private GameEntity.Spell[] m_hitSpells = new GameEntity.Spell[4];
    private GameEntity.Spell[] m_healSpells = new GameEntity.Spell[4];

    private int _nbHitSpell = 0;
    private int _nbHealSpell = 0;
    private CircleJauge _circleJaugeHealth;
    private CircleJauge _circleJaugeSpell;
    private Text _textTimer;
    private IEnumerator _coroutineSpell;
        
    public float _timer { get; private set; }
    public int playerLife { get; private set; }
        
    #endregion

    #region Manager implementation

    protected override IEnumerator InitCoroutine()
    {
        _circleJaugeHealth = m_playerJauge.GetComponent<CircleJauge>();
        _circleJaugeSpell = m_currentSpellJauge.GetComponent<CircleJauge>();
        _textTimer = m_panelTimer.GetComponentInChildren<Text>();
        m_panelNameLevel.GetComponentInChildren<Text>().text = "Name level : " + LevelManager.Instance.IdLevel;

        m_panelBestScore.GetComponentInChildren<Text>().text = "Best Scrore : " + TimeFormatterTools.FromSecondsToTime(
            PlayerPrefs.GetFloat(GameManager.Instance.m_IdPlayer + ".BESTSCORE." + LevelManager.Instance.IdLevel, 0)
        );
        StartCoroutine(UpdateTime());

        EventManager.Instance.Raise(new HudManagerReadyEvent());
        yield break;
    }
        
    public override void SubscribeEvents()
    {
        EventManager.Instance.AddListener<AddSpellEvent>(AddSpell);
        EventManager.Instance.AddListener<DeleteSpellEvent>(DeleteSpell);
        EventManager.Instance.AddListener<UpdateHealth>(UpdateHealth);
        //EventManager.Instance.AddListener<IsPoisonedEvent>(AddPoison);
        EventManager.Instance.AddListener<TryAgainGameEvent>(InitStatsEvent);
        EventManager.Instance.AddListener<SpellLaunchEvent>(DisplaySpellLaunched);
        base.SubscribeEvents();
    }

    public override void UnsubscribeEvents()
    {
        EventManager.Instance.RemoveListener<AddSpellEvent>(AddSpell);
        EventManager.Instance.RemoveListener<DeleteSpellEvent>(DeleteSpell);
        EventManager.Instance.RemoveListener<UpdateHealth>(UpdateHealth);
        //EventManager.Instance.RemoveListener<IsPoisonedEvent>(AddPoison);
        EventManager.Instance.RemoveListener<TryAgainGameEvent>(InitStatsEvent);
        EventManager.Instance.RemoveListener<SpellLaunchEvent>(DisplaySpellLaunched);


        base.UnsubscribeEvents();
    }
        
    #endregion

    #region Callbacks to GameManager events

    protected override void GameStatisticsChanged(GameStatisticsChangedEvent e)
    {
        //TO DO
    }
        
    #endregion

    #region Callbacks to boardManager Events

    protected override void GameOver(GameOverEvent e)
    {
        SaveBestScore();
    }
    protected override void GameVictory(GameVictoryEvent e)
    {
        SaveBestScore();
    }
        

    #endregion

    #region Callbacls to Spell events

    private void DisplaySpellLaunched(SpellLaunchEvent e)
    {
        float duree = 1.5f;
        _coroutineSpell = ChangeCurrentSpell(duree,e.eSpell);
        StartCoroutine(_coroutineSpell);
        UpdateSpellUses(e.eSpell,false);
    }

    #endregion
        
    #region Callbacks to Entity events

    private void AddSpell(AddSpellEvent addSpellEvent)
    {
        if (addSpellEvent.hit && _nbHitSpell < m_hitSpells.Length)
        {
            int index = FindFreeIndexFrom(m_hitSpells);
            if(index==-1)
                return;
            Image im = m_panelHitSpells.FindComponentsInChildWithTag<Image>("imHitSpell")[index];
            im.sprite = addSpellEvent.spell.m_SpellSprite;
            var tempColor = im.color;
            tempColor.a = 1f;
            im.color = tempColor;
            m_hitSpells[index] = addSpellEvent.spell;
            UpdateSpellUses(addSpellEvent.spell,true);
            _nbHitSpell++;
        }
        else if(!addSpellEvent.hit && _nbHealSpell < m_healSpells.Length)
        {
            int index = FindFreeIndexFrom(m_healSpells);
            if(index==-1)
                return;
            Image im = m_panelHealSpells.FindComponentsInChildWithTag<Image>("imHealSpell")[index];
            im.sprite = addSpellEvent.spell.m_SpellSprite;
            var tempColor = im.color;
            tempColor.a = 1f;
            im.color = tempColor;
            m_healSpells[index] = addSpellEvent.spell;
            UpdateSpellUses(addSpellEvent.spell,true);
            _nbHealSpell++;
        }
            
    }

    private void DeleteSpell(DeleteSpellEvent deleteSpellEvent)
    {
        GameEntity.Spell sp = deleteSpellEvent.spell;
        for (int i = 0; i < m_hitSpells.Length; i++)
        {
            if (m_hitSpells[i] == sp)
            {
                DeleteSpell("imHitSpell",m_hitSpells,m_panelHitSpells, i);
                DeleteSpellNumber("hitSpellUseNumber",m_panelHitSpells, i);
                _nbHitSpell--;
            }

            if (m_healSpells[i] == sp)
            {
                DeleteSpell("imHealSpell",m_healSpells,m_panelHealSpells,i);
                DeleteSpellNumber("healSpellUseNumber",m_panelHealSpells, i);
                _nbHealSpell--;
            }
        }
    }

    private void UpdateHealth(UpdateHealth updateHealth)
    {
        playerLife = (int) updateHealth.newHealth;
        Debug.LogWarning("circle jauge : "+_circleJaugeHealth);
        _circleJaugeHealth.m_Value = 200/updateHealth.maxHealth * updateHealth.newHealth;
        if (playerLife <= 0)
        {
            StartCoroutine(WaitCoroutineGameOver());
        }
    }

    /*private void AddPoison(IsPoisonedEvent isPoisonedEvent)
        {
            float duree = isPoisonedEvent.duration;
            _coroutinePoison = applyPoisonCoroutine(duree);
            StartCoroutine(_coroutinePoison);
        }*/

    #endregion

    #region Coroutines

    private IEnumerator WaitCoroutineGameOver()
    {
        yield return new WaitForSeconds(3);
        EventManager.Instance.Raise(new GameOverEvent());
    }

    private IEnumerator ChangeCurrentSpell(float duree, GameEntity.Spell eESpell)
    {
        Image currentSpellIm = m_currentSpellJauge.GetComponentsInChildren<Image>()[2];
        var tempColor = currentSpellIm.color;
        tempColor.a = 1f;
        currentSpellIm.color = tempColor;
        currentSpellIm.sprite = eESpell.m_SpellSprite;
        float elapsedTime = 0;
        while (elapsedTime < duree)
        {
            _circleJaugeSpell.m_Value = 200/duree*elapsedTime;
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        tempColor.a = 0f;
        currentSpellIm.color = tempColor;
    }

    private IEnumerator UpdateTime()
    {
        for (;;)
        {
            string time = TimeFormatterTools.FromSecondsToTime(_timer);
            _textTimer.text = "Time : "+ time;
            _timer++;
            yield return new WaitForSeconds(1f);
        }
    }
    #endregion
        
    #region Callback to MenuManager events
        
    protected void InitStatsEvent(TryAgainGameEvent e)
    {
        InitStats();
    }
        
    #endregion
        
    #region utils

    private void SaveBestScore()
    {
        float currentBestScore = PlayerPrefs.GetFloat(GameManager.Instance.m_IdPlayer + ".BESTSCORE." + LevelManager.Instance.IdLevel, 0);
        float currentTimer = _timer;
        Debug.Log("Best score : " + currentBestScore + " // current score : " + currentTimer);
        if (!(currentBestScore > currentTimer) && currentBestScore != 0) return;
        Debug.Log("update best score");
        PlayerPrefs.SetFloat(GameManager.Instance.m_IdPlayer + ".BESTSCORE." + LevelManager.Instance.IdLevel, currentTimer);
        PlayerPrefs.Save();
    }
        
    private int FindFreeIndexFrom(GameEntity.Spell[] tab)
    {
        for (int i = 0; i < tab.Length; i++)
            if (!tab[i])
                return i;
        return -1;
    }

    private void InitStats()
    {
        _timer = 0;
        playerLife = 100; // par défaut
        _circleJaugeSpell.m_Value = 0;
        _circleJaugeHealth.m_Value = 200;
        DeleteAllSpells();
        _nbHitSpell = 0;
        _nbHealSpell = 0;
        StopCoroutine(_coroutineSpell);
        string time = TimeFormatterTools.FromSecondsToTime(PlayerPrefs.GetFloat(GameManager.Instance.m_IdPlayer + ".BESTSCORE." + LevelManager.Instance.IdLevel, 0));
        m_panelBestScore.GetComponentInChildren<Text>().text = "Best Scrore : " + time;
    }

    private void DeleteSpell(string name, GameEntity.Spell[] tabSpells, GameObject panel, int index)
    {
        tabSpells[index] = null;
        Image im = panel.FindComponentsInChildWithTag<Image>(name)[index];
        var tempColor = im.color;
        tempColor.a = 0f;
        im.color = tempColor;
    }

    private void DeleteSpellNumber(string name, GameObject panel, int index)
    {
        Debug.Log("Delete spell : " + name);
        Text nbUsesText = panel.FindComponentsInChildWithTag<Text>(name)[index]; 
        Debug.Log("actual uses = " + nbUsesText.text);
        nbUsesText.text = "";
    }

    private void DeleteAllSpells()
    {
        for (int i = 0; i < m_hitSpells.Length; i++)
        {
            DeleteSpell("imHitSpell",m_hitSpells,m_panelHitSpells,i);
        }
        for (int i = 0; i < m_healSpells.Length; i++)
        {
            DeleteSpell("imHealSpell",m_healSpells,m_panelHealSpells,i);
        }
            
    }

    private Text FindTextNbUsesSpell(GameEntity.Spell s)
    {
        for (int i = 0; i < m_hitSpells.Length; i++)
        {
            if (m_hitSpells[i] == s)
            {
                return m_panelHitSpells.FindComponentsInChildWithTag<Text>("hitSpellUseNumber")[i];
            }

            if (m_healSpells[i] == s)
            {
                return m_panelHealSpells.FindComponentsInChildWithTag<Text>("healSpellUseNumber")[i];
            }
        }

        return null;
    }

    private void UpdateSpellUses(GameEntity.Spell s, bool init)
    {
        Text nbUsesText = FindTextNbUsesSpell(s);
        int nbUses = s.m_SpellUsageCount;
        if(!init) nbUses-- ;
        if (nbUses > 9)
            nbUsesText.text = "9+";
        else
            nbUsesText.text = ""+nbUses;
    }

    #endregion
}