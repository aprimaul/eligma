using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using UnityEngine;
using UnityEngine.Tilemaps;
using Vector3 = UnityEngine.Vector3;

namespace Common
{
    public sealed class MapMatrix
    {
        private Matrix3x2 m_Positions;
        private Vector3Int m_Size;

        private int[][][] m_Matrix;

        public static MapMatrix CreateEmpty()
        {
            return new MapMatrix(BoardManager.Instance.BoardMatrix);
        }

        private MapMatrix(MapMatrix m)
        {
            m_Size = m.m_Size;
            m_Matrix = Create3DArray(m_Size.z, m_Size.y, m_Size.x);
        }

        public MapMatrix(Tilemap[] t)
        {
            var stages = StageSeparation(t);
            var statesPositions = GetStagePositions(stages);
            m_Positions = GetMapPositions(statesPositions);
            m_Size = GetSize(m_Positions);
            m_Matrix = GetMapMatrix(stages);
        }

        #region Size and structure preparation

        private Dictionary<int, Dictionary<string, Dictionary<Tilemap, Matrix3x2>>> StageSeparation(Tilemap[] tilemaps)
        {
            var stages = new Dictionary<int, Dictionary<string, Dictionary<Tilemap, Matrix3x2>>>();

            foreach (Tilemap tilemap in tilemaps)
            {
                String[] name = tilemap.name.Split(':');

                int stage = Int32.Parse(name[0]);

                if (!stages.ContainsKey(stage))
                {
                    stages.Add(stage, new Dictionary<string, Dictionary<Tilemap, Matrix3x2>>());
                }

                if (!stages[stage].ContainsKey(name[1]))
                {
                    stages[stage].Add(name[1], new Dictionary<Tilemap, Matrix3x2>());
                }

                BoundsInt cellBounds = tilemap.cellBounds;
                Vector3Int tmSize = cellBounds.size;
                Vector3Int tmPos = cellBounds.position;

                Matrix3x2 tmMatrix = FromVectors(tmPos, tmPos + tmSize);
                stages[stage][name[1]].Add(tilemap, tmMatrix);
            }

            return stages;
        }

        private Dictionary<int, Matrix3x2> GetStagePositions(
            Dictionary<int, Dictionary<string, Dictionary<Tilemap, Matrix3x2>>> stages)
        {
            var stagePositions = new Dictionary<int, Matrix3x2>();

            foreach (var stage in stages)
            {
                Matrix3x2 stageMatrix = InitMinMaxMatrix();

                stageMatrix = (stage.Value).Values
                    .SelectMany(map => map.Values)
                    .Aggregate(stageMatrix, (current, m) => Merge(current, m));

                stagePositions.Add(stage.Key, stageMatrix);
            }
            
            return stagePositions;
        }

        private Matrix3x2 GetMapPositions(Dictionary<int, Matrix3x2> stageSizes)
        {
            Matrix3x2 fullPositions = InitMinMaxMatrix();

            fullPositions = stageSizes.Aggregate(fullPositions,
                (current, stageSize) => Merge(current, stageSize.Value));

            fullPositions.M31 = 0;
            fullPositions.M32 = stageSizes.Count;

            return fullPositions;
        }

        #endregion

        #region Tilemaps to matrix
        private int[][][] GetMapMatrix(Dictionary<int, Dictionary<string, Dictionary<Tilemap, Matrix3x2>>> stages)
        {
            int[][][] mapMatrix = Create3DArray(m_Size.z, m_Size.y, m_Size.x);
            
            for (int i = (m_Size.z - 1); i >= 0; i--)
            {
                foreach (var maps in stages[i])
                {
                    foreach (var map in maps.Value)
                    {
                        TransformTilemapToMatrix(map.Key, m_Positions, maps.Key.Equals("stairs") ? 3 : 1, mapMatrix[i]);
                    }
                }

                if (i < m_Size.z - 1)
                    MergeMapMatrices(mapMatrix, i, i + 1);

                
            }
            
            mapMatrix = AntiClockwiseRotation(mapMatrix);

            VerticalMirrorRotation(mapMatrix);

            HorizontalMirrorRotation(mapMatrix);

            return mapMatrix;
        }

        private void TransformTilemapToMatrix(Tilemap t, Matrix3x2 m, int value, int[][] stageMatrix)
        {
            TileBase[] tiles = t.GetTilesBlock(new BoundsInt(Vector3Int.CeilToInt(GetPositionLeftVector3(m)),
                new Vector3Int(m_Size.x, m_Size.y, 1)));

            for (int i = 0; i < m_Size.y; i++)
            {
                for (int j = 0; j < m_Size.x; j++)
                {
                    TileBase tile = tiles[m_Size.x * i + j];
                    if (tile != null)
                    {
                        stageMatrix[i][j] = Math.Max(value, stageMatrix[i][j]);
                    }
                }
            }
        }

        #endregion

        #region Debug

        public void DisplayAll(String id = "/")
        {
            for (int i = 0; i < m_Size.z; i++)
            {
                DisplayStage(i, id);
            }
        }

        public void DisplayStage(int stage, String id = "-")
        {
            Debug.LogWarning("Stage " + id + " " + stage);
            DisplayArray(m_Matrix[stage], m_Size.y, m_Size.x);
        }

        public void DisplayArray(int[][] a, int x, int y)
        {

            {
                String str = "    ";
                for (int j = 0; j < y; j++)
                {
                    str += j < 10 ? j + "  " : j+" ";
                }
                Debug.Log(str);
            }

            for (int i = 0; i < x; i++)
            {
                String str = i < 10 ? " "+i+"  " : i+"  ";
                for (int j = 0; j < y; j++)
                {
                    int val = a[i][j];
                    str += (val == 0 ? "   " : val == 1 ? ".  " : val == 3 ? "#  " : val+"  ");
                }

                Debug.Log(str);
            }
            
            {
                String str = "    ";
                for (int j = 0; j < y; j++)
                {
                    str += j < 10 ? j + "  " : j+" ";
                }
                Debug.Log(str);
            }
        }

        #endregion

        #region Map Matrix complex operations

        /// <summary>
        /// Usage with first matrix form only !
        /// </summary>
        /// <param name="mapMatrix"></param>
        /// <param name="stageA"></param>
        /// <param name="stageB"></param>
        private void MergeMapMatrices(int[][][] mapMatrix, int stageA, int stageB)
        {
            int source = Math.Max(stageA, stageB);
            int destination = Math.Min(stageA, stageB);

            for (int i = 0; i < m_Size.y-1; i++)
            {
                for (int j = 0; j < m_Size.x-1; j++)
                {
                    int sourceVal = mapMatrix[source][i+1][j+1];
                    int destVal = mapMatrix[destination][i][j];
                    mapMatrix[destination][i][j] = sourceVal == 3 ? 3 : sourceVal >= 1 ? 2 : destVal;
                }
            }
        }

        private void HorizontalMirrorRotation(int[][][] m)
        {
            for (int stage = 0; stage < m_Size.z; stage++)
            {
                for (int i = 0, k = m_Size.y-1; i < m_Size.y/2; i++, k--)
                {
                    for (int j = 0; j < m_Size.x; j++)
                    {
                        int old = m[stage][i][j];
                        m[stage][i][j] = m[stage][k][j];
                        m[stage][k][j] = old;
                    }
                }
            }
        }

        private void VerticalMirrorRotation(int[][][] m)
        {
            for (int stage = 0; stage < m_Size.z; stage++)
            {
                for (int i = 0; i < m_Size.y; i++)
                {
                    for (int j = 0, k = m_Size.x-1; j < m_Size.x/2; j++, k--)
                    {
                        int old = m[stage][i][j];
                        m[stage][i][j] = m[stage][i][k];
                        m[stage][i][k] = old;
                    }
                }
            }
        }

        private int[][][] AntiClockwiseRotation(int[][][] m)
        {
            InvertPositionValuesX_Y();
            InvertSizeValueX_Y();

            int[][][] mapMatrix = Create3DArray(m_Size.z, m_Size.y, m_Size.x);

            for (int k = 0; k < m_Size.z; k++)
            {
                for (int i = 0; i < m_Size.y; i++)
                {
                    for (int j = 0; j < m_Size.x; j++)
                    {
                        mapMatrix[k][i][j] = m[k][j][m_Size.y-1-i];
                    }
                }
            }
            
            return mapMatrix;
        }

        #endregion

        #region Map matrix access

        public Vector3Int GetSize()
        {
            return m_Size;
        }

        public Vector3 GetOriginPosition()
        {
            return GetPositionLeftVector3(m_Positions);
        }

        public bool IsFree(int x, int y, int z = 0)
        {
            return GetValue(x,y,z) == 1;
        }
        
        public bool IsFree(Vector3Int v)
        {
            return IsFree(v.x, v.y, v.z);
        }
        
        public bool IsStairs(int x, int y, int z = 0)
        {
            return GetValue(x,y,z) == 3;
        }
        
        public bool IsStairs(Vector3Int v)
        {
            return IsStairs(v.x, v.y, v.z);
        }

        public int GetValue(int x, int y, int z)
        {
            if (m_Size.x <= m_Size.x - x || m_Size.y <= y || m_Size.z <= z || m_Size.x - x < 0 || y < 0 || z < 0)
            {
                return -1;
            }
            return m_Matrix[z][y][m_Size.x - x];
        }

        public int GetValue(Vector3Int v)
        {
            return GetValue(v.x, v.y, v.z);
        }

        #endregion

        #region Map matrix update

        public void AddEntity(Vector3Int v)
        {
            SetValue(v, 4);
        }

        public void MoveEntity(Vector3Int from, Vector3Int to)
        {
            SetValue(from, GetValue(from) == 5 ? 4 : 1);
            SetValue(to, GetValue(to) == 4 ? 5 : 4);
        }
        
        public void SetValue(int x, int y, int z, int value)
        {
            m_Matrix[z][y][m_Size.x - x] = value;
        }

        public void SetValue(Vector3Int v, int value)
        {
            SetValue(v.x, v.y, v.z, value);
        }

        #endregion
        
        #region Matrix, array and vectors tools functions
        private static int[][][] Create3DArray(int x, int y, int z)
        {
            int[][][] mapMatrix = new int[x][][];
            for (int i = 0; i < x; i++)
            {
                mapMatrix[i] = new int[y][];
                for (int j = 0; j < y; j++)
                {
                    mapMatrix[i][j] = new int[z];
                }
            }

            return mapMatrix;
        }

        private void SwapPositionValues(bool xAxis, bool yAxis)
        {
            if (xAxis)
            {
                float oldX = m_Positions.M11;
                m_Positions.M11 = m_Positions.M12;
                m_Positions.M12 = oldX;
            }

            if (!yAxis) return;
            float oldY = m_Positions.M21;
            m_Positions.M21 = m_Positions.M22;
            m_Positions.M22 = oldY;
        }

        private void InvertPositionValuesX_Y()
        {
            float old1 = m_Positions.M11;
            m_Positions.M11 = m_Positions.M21;
            m_Positions.M21 = old1;
            
            float old2 = m_Positions.M12;
            m_Positions.M12 = m_Positions.M22;
            m_Positions.M22 = old2;
        }

        private void InvertSizeValueX_Y()
        {
            int old = m_Size.x;
            m_Size.x = m_Size.y;
            m_Size.y = old;
        }

        private static Matrix3x2 InitMinMaxMatrix()
        {
            return new Matrix3x2(10000, -10000, 10000, -10000, 10000, -10000);
        }

        private static Matrix3x2 Merge(Matrix3x2 m1, Matrix3x2 m2)
        {
            Vector3 minValue = Vector3.Min(GetPositionLeftVector3(m1), GetPositionLeftVector3(m2));
            Vector3 maxValue = Vector3.Max(GetPositionRightVector3(m1), GetPositionRightVector3(m2));
            return FromVectors(minValue, maxValue);
        }

        private static Matrix3x2 FromVectors(Vector3Int posMin, Vector3Int posMax)
        {
            return new Matrix3x2(posMin.x, posMax.x, posMin.y, posMax.y,posMin.z, posMax.z); 
        }

        private static Matrix3x2 FromVectors(Vector3 posMin, Vector3 posMax)
        {
            return new Matrix3x2(posMin.x, posMax.x, posMin.y, posMax.y,posMin.z, posMax.z); 
        }

        private static Vector3 GetPositionRightVector3(Matrix3x2 m)
        {
            return new Vector3(m.M12, m.M22, m.M32);
        }

        private static Vector3 GetPositionLeftVector3(Matrix3x2 m)
        {
            return new Vector3(m.M11, m.M21, m.M31);
        }

        private static Vector3Int GetSize(Matrix3x2 m)
        {
            return Vector3Int.CeilToInt(CustomVector3.Abs(GetPositionRightVector3(m) - GetPositionLeftVector3(m)));
        }

        #endregion
    }
}