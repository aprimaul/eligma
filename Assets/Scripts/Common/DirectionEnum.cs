namespace Common
{
    public enum Direction
    {
        North = 1,
        South = 2,
        West = 4,
        East = 8,
        NorthWest = 16,
        NorthEast = 32,
        SouthWest = 64,
        SouthEast = 128,
        _Cross = 15,
        _Diagonal = 240,
        _FrontBehind = 3,
        _Beside = 12,
        _NorthDiagonal = 48,
        _SouthDiagonal = 192,
        _LeftDiagonal = 80,
        _RightDiagonal = 160
    }
}