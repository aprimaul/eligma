using System;
using UnityEngine;

namespace Common
{
    public class CustomVector3
    {
        // Store map vector
        private Vector3Int m_mapVector;

        private CustomVector3(Vector3 mapVector3) : this(mapVector3.x, mapVector3.y, mapVector3.z) { }
        
        private CustomVector3(float x, float y, float z)
        {
            m_mapVector = new Vector3Int((int)x, (int)y, (int)z);
        }

        public static CustomVector3 FromGeometricVector2(Vector2 v, int layer = 0)
        {
            v.x -= 0.5f;
            return new CustomVector3(-(v.x*1f - v.y*2f), v.y*2f + v.x*1f, layer);
        }

        public static CustomVector3 FromMapVector3(Vector3Int v)
        {
            return new CustomVector3(v.x, v.y, v.z);
        }

        public static CustomVector3 FromMatrixVector3(Vector3Int v)
        {
            return new CustomVector3(BoardManager.Instance.BoardMatrix.GetOriginPosition() + v);
        }

        public Vector3 ToGeometricVector3()
        {
            // Inversion du sens de x
            return new Vector3(
                (-m_mapVector.x+m_mapVector.y)/2f, 
                (m_mapVector.y+m_mapVector.x)/4f, 
                0);
        }

        public Vector3Int ToMapVector3()
        {
            return Vector3Int.CeilToInt(m_mapVector);
        }

        public Vector3Int ToMatrixVector3()
        {
            Vector3 custo = m_mapVector;
            m_mapVector.x = -m_mapVector.x;
            
            return Vector3Int.CeilToInt(Abs(BoardManager.Instance.BoardMatrix.GetOriginPosition() - custo));
        }
        
        public static Vector3 Abs(Vector3 v)
        {
            return new Vector3(Math.Abs(v.x), Math.Abs(v.y), Math.Abs(v.z));
        }
        
        
    }
}