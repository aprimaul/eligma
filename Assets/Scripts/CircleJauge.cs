﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class CircleJauge : MonoBehaviour
{
    [FormerlySerializedAs("_jauge")] public Image m_Jauge;

    [FormerlySerializedAs("_value")] public float m_Value;

    private void Awake()
    {
        m_Value = 200;
    }

    // Update is called once per frame
    private void Update()
    {
        ValueChange(m_Value);
    }

    private void ValueChange(float value)
    {
        float amount = (value / 100.0f) * 180.0f / 360;
        m_Jauge.fillAmount = amount;
    }
}
