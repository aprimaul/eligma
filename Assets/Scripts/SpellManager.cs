using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common;
using SDD.Events;
using Tools;
using UnityEngine;

public class SpellManager : Manager<SpellManager>
{
    public override void SubscribeEvents()
    {
        base.SubscribeEvents();
        EventManager.Instance.AddListener<LaunchSpell>(DispatchSpell);
    }

    public override void UnsubscribeEvents()
    {
        base.UnsubscribeEvents();
        EventManager.Instance.RemoveListener<LaunchSpell>(DispatchSpell);
    }
    protected override IEnumerator InitCoroutine()
    {
        yield return null;
    }

    private void DispatchSpell(LaunchSpell e)
    {
        List<Vector3Int> spellPositions = CreateSpellTargets(e);
        RotateSpellTargets(spellPositions, e.eEntity.m_dir);
            
        MapMatrix m = BoardManager.Instance.BoardMatrix;

        EventManager.Instance.Raise(
            new SpellLaunchEvent {
                eEntity = e.eEntity, eSpell = e.eSpell, 
                ePositions = spellPositions
                    .Where(spellPosition => m.GetValue(spellPosition) == 4)
                    .ToArray(),
                hitArray = e.hitArray,
                spellArrayIndex = e.spellArrayIndex
            });
    }

    private List<Vector3Int> CreateSpellTargets(LaunchSpell e)
    {
        List<Vector3Int> spellPositions = new List<Vector3Int>();
        Vector3Int vPosition = e.eEntity.MatrixPosition;
            
        int stage = vPosition.z;

        Debug.LogWarning("Stage  = "+stage);
        spellPositions.Add(vPosition + new Vector3Int(-1, 1, stage));
        spellPositions.Add(vPosition + new Vector3Int(1, 1, stage));
        spellPositions.Add(vPosition + new Vector3Int(-1, -1, stage));
        spellPositions.Add(vPosition + new Vector3Int(1, -1, stage));
            
        for (int i = e.eSpell.m_RangeStart; i <= e.eSpell.m_RangeEnd; i++)
        {
            spellPositions.Add(vPosition + new Vector3Int(0 , i, stage)); // North
            //spellPositions.Add(vPosition + new Vector3Int(0 , -i, stage)); // S
            spellPositions.Add(vPosition + new Vector3Int(-i , 0, stage)); // W
            spellPositions.Add(vPosition + new Vector3Int(i , 0, stage)); // E
            for (int j = 1; j < i; j++)
            {
                spellPositions.Add(vPosition + new Vector3Int(j, i-j, stage)); //NE
                spellPositions.Add(vPosition + new Vector3Int(-j, i-j, stage)); //NW
                //spellPositions.Add(vPosition + new Vector3Int(j, -(i-j), stage)); //SE
                //spellPositions.Add(vPosition + new Vector3Int(-j, -(i-j), stage)); //SW
            }
        }

        return spellPositions;
    }

    private void RotateSpellTargets(List<Vector3Int> spellsPositions, Direction d)
    {
        if(d == Direction.North)
            return;

        Vector3Int rotationVector = DirectionTools.DirectionToRotationVector(d);
        bool invertXY = rotationVector.x + rotationVector.y != 0;

        for (int i = 0; i < spellsPositions.Count; i++)
        {
            Vector3Int v = spellsPositions[i];
            if (invertXY)
            {
                int old = v.x;
                v.x = v.y;
                v.y = old;
            }
            v = new Vector3Int(rotationVector.x*v.x, rotationVector.y*v.y, v.z);
            spellsPositions[i] = v;
        }
    }

    private void DisplayCaseHit(float x, float y)
    {
        GameObject go = new GameObject("caseHit");
        go.transform.position = new UnityEngine.Vector3(x,y,500);
        go.transform.localScale = new UnityEngine.Vector3(2,2,2);
        Debug.LogWarning("postion sprite to display : " + go.transform.position);
        go.AddComponent<SpriteRenderer>();
        SpriteRenderer sprender = go.GetComponent<SpriteRenderer>();
        sprender.sprite = Resources.Load<Sprite>("Sprites/hitCase");
    }
}