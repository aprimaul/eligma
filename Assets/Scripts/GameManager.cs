﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Common;
using SDD.Events;
using Tools;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameState
{
    gamePlayerMenu,
    gameCreatePlayer,
    gameWorldMenu,
    gameScrollPlayerMenu,
    gameMainMenu,
    gamePlay,
    gameLevelMenu,
    gameNextLevel,
    gamePause,
    gameOver,
    gameVictory
}

public class GameManager : Manager<GameManager>
{
    #region Game State

    private GameState m_GameState;

    public bool IsPlaying
    {
        get { return m_GameState == GameState.gamePlay; }
    }

    #endregion

    //LIVES

    #region Lives

    /*[Header("GameManager")] [SerializeField]
        private int m_NStartLives;

        private int m_NLives;

        public int NLives
        {
            get { return m_NLives; }
        }

        void DecrementNLives(int decrement)
        {
            SetNLives(m_NLives - decrement);
        }

        void SetNLives(int nLives)
        {
            m_NLives = nLives;
            EventManager.Instance.Raise(new GameStatisticsChangedEvent()
                {eBestScore = BestScore, eScore = m_Score, eNLives = m_NLives});
        }*/

    #endregion


    #region Score

    /*private float m_Score;

        public float Score
        {
            get { return m_Score; }
            set
            {
                m_Score = value;
                BestScore = Mathf.Max(BestScore, value);
            }
        }

        public float BestScore
        {
            get { return PlayerPrefs.GetFloat("BEST_SCORE", 0); }
            set { PlayerPrefs.SetFloat("BEST_SCORE", value); }
        }*/
        

    /*void IncrementScore(float increment)
        {
            SetScore(m_Score + increment);
        }

        void SetScore(float score, bool raiseEvent = true)
        {
            Score = score;

            if (raiseEvent)
                EventManager.Instance.Raise(new GameStatisticsChangedEvent()
                    {eBestScore = BestScore, eScore = m_Score, eNLives = m_NLives});
        }*/
        
    public int BestScore
    {
        get { return PlayerPrefs.GetInt(m_IdPlayer+".BEST_SCORE."+LevelManager.Instance.IdLevel, 0); }
        set { PlayerPrefs.SetInt(m_IdPlayer+".BEST_SCORE."+LevelManager.Instance.IdLevel, value); }
    }

    #endregion

    #region Time

    void SetTimeScale(float newTimeScale)
    {
        Time.timeScale = newTimeScale;
    }

    #endregion

    public int m_IdPlayer { get; private set; }

    #region Events' subscription

    public override void SubscribeEvents()
    {
        //TODO: A deplacer


        base.SubscribeEvents();

        // MenuManager
        EventManager.Instance.AddListener<MainMenuButtonClickedEvent>(MainMenuButtonClicked);
        EventManager.Instance.AddListener<ChoosePlayerButtonClickedEvent>(ChoosePlayerButtonClicked);
        EventManager.Instance.AddListener<CreatePlayerButtonClickedEvent>(CreatePlayerButtonClicked);
        EventManager.Instance.AddListener<PlayButtonClickedEvent>(PlayButtonClicked);
        EventManager.Instance.AddListener<ResumeButtonClickedEvent>(ResumeButtonClicked);
        EventManager.Instance.AddListener<EscapeButtonClickedEvent>(EscapeButtonClicked);
        //EventManager.Instance.AddListener<QuitButtonClickedEvent>(QuitButtonClicked);
        EventManager.Instance.AddListener<ScrollMenuPlayerButtonClickedEvent>(ScrollMenuPlayerButton);
        EventManager.Instance.AddListener<WorldSelectionButtonClickedEvent>(WorldSelectionButtonClicked);
        EventManager.Instance.AddListener<ConfirmQuitGameButtonClickedEvent>(ConfirmQuitGameButtonClicked);
        EventManager.Instance.AddListener<CreditsButtonClickedEvent>(CreditsButtonClicked);
        EventManager.Instance.AddListener<QuitOptionClickedEvent>(QuitAndSaveOption);
        EventManager.Instance.AddListener<TryAgainGameEvent>(RestartGameEvent);

        //Score Item
        //EventManager.Instance.AddListener<ScoreItemEvent>(ScoreHasBeenGained);
    }

    public override void UnsubscribeEvents()
    {
        base.UnsubscribeEvents();

        // MenuManager
        EventManager.Instance.RemoveListener<MainMenuButtonClickedEvent>(MainMenuButtonClicked);
        EventManager.Instance.RemoveListener<ChoosePlayerButtonClickedEvent>(ChoosePlayerButtonClicked);
        EventManager.Instance.RemoveListener<CreatePlayerButtonClickedEvent>(CreatePlayerButtonClicked);
        EventManager.Instance.RemoveListener<ResumeButtonClickedEvent>(ResumeButtonClicked);
        EventManager.Instance.RemoveListener<EscapeButtonClickedEvent>(EscapeButtonClicked);
        EventManager.Instance.RemoveListener<PlayButtonClickedEvent>(PlayButtonClicked);
        /*EventManager.Instance.RemoveListener<QuitButtonClickedEvent>(QuitButtonClicked);*/
        EventManager.Instance.RemoveListener<CreatePlayerButtonClickedEvent>(CreatePlayerButtonClicked);
        EventManager.Instance.RemoveListener<ScrollMenuPlayerButtonClickedEvent>(ScrollMenuPlayerButton);
        EventManager.Instance.RemoveListener<WorldSelectionButtonClickedEvent>(WorldSelectionButtonClicked);
        EventManager.Instance.RemoveListener<ConfirmQuitGameButtonClickedEvent>(ConfirmQuitGameButtonClicked);
        EventManager.Instance.RemoveListener<CreditsButtonClickedEvent>(CreditsButtonClicked);
        EventManager.Instance.RemoveListener<QuitOptionClickedEvent>(QuitAndSaveOption);
        EventManager.Instance.RemoveListener<TryAgainGameEvent>(RestartGameEvent);

        //Score Item
        //EventManager.Instance.RemoveListener<ScoreItemEvent>(ScoreHasBeenGained);
    }

    #endregion

    #region Manager implementation

    protected override IEnumerator InitCoroutine()
    {
        //TODO:Regarder comment ça marche
        //Menu();
        if (MusicLoopsManager.Instance) MusicLoopsManager.Instance.PlayMusic(Constants.MENU_MUSIC);
        InitNewGame(); // essentiellement pour que les statistiques du jeu soient mise à jour en HUD
        yield break;
    }

    #endregion

    #region Game flow & Gameplay

    //Game initialization
    void InitNewGame(bool raiseStatsEvent = true)
    {
        Application.targetFrameRate = 40;
        QualitySettings.vSyncCount = 0;
        //SetScore(0);
    }

    #endregion

    #region Callbacks to events issued by BoardManager
        
    protected override void GameOver(GameOverEvent e)
    {
        SetTimeScale(0);
        m_GameState = GameState.gameOver;
        if(SfxManager.Instance) SfxManager.Instance.PlaySfx2D(Constants.GAMEOVER_SFX);
    }
        
    protected override void GameVictory(GameVictoryEvent e)
    {
        SetTimeScale(0);
        m_GameState = GameState.gameVictory;
        if(SfxManager.Instance) SfxManager.Instance.PlaySfx2D(Constants.GAMEVICTORY_SFX);
        Debug.Log("Game victory catch by Game manager");
    }
        
    #endregion
        
    #region Callbacks to events issued by Score items

    /*private void ScoreHasBeenGained(ScoreItemEvent e)
        {
            if (IsPlaying)
                IncrementScore(e.eScore);
        }*/

    #endregion

    #region Callbacks to Events issued by MenuManager

    private void RestartGameEvent(TryAgainGameEvent e)
    {
        Play();
    }
        
    private void QuitAndSaveOption(QuitOptionClickedEvent e)
    {
        SaveOptions(e.volume);
    }

    private void CreditsButtonClicked(CreditsButtonClickedEvent e)
    {
        ShowCredits();
    }
        
    /// <summary>
    /// Action after click on welcome menu
    /// </summary>
    /// <param name="e"></param>
    private void MainMenuButtonClicked(MainMenuButtonClickedEvent e)
    {
        PlayerMenu();
    }

    private void ScrollMenuPlayerButton(ScrollMenuPlayerButtonClickedEvent e)
    {
        ScrollMenuPlayer();
    }

    /// <summary>
    /// Action after click on a choose player button. If the player doesn't exist, will raise ask for an input
    /// </summary>
    /// <param name="e"></param>
    private void ChoosePlayerButtonClicked(ChoosePlayerButtonClickedEvent e)
    {
        //TODO: On a le droit ?
        m_IdPlayer = e.eIdButton;
        if (e.eCreatePlayer)
            PlayerCreateInput();
        else
            WorldMenu();
    }

    /// <summary>
    /// Action after the "OK" button has been clicked on the "Choose your name" Sub UI. 
    /// </summary>
    /// <param name="e"></param>
    private void CreatePlayerButtonClicked(CreatePlayerButtonClickedEvent e)
    {
        PlayerCreate(m_IdPlayer, e.eNamePlayer);
    }
        
    private void ConfirmQuitGameButtonClicked(ConfirmQuitGameButtonClickedEvent e)
    {
        Application.Quit();
    }

    /// <summary>
    /// Action after the "Escape" button has been clicked during a game
    /// </summary>
    /// <param name="e"></param>
    private void EscapeButtonClicked(EscapeButtonClickedEvent e)
    {
        if (IsPlaying)
        {
            Pause();
        }
    }
		
    /*private void PlayButtonClicked(PlayButtonClickedEvent e)
		{
			Play();
		}

        
        
        /*

    
        private void QuitButtonClicked(QuitButtonClickedEvent e)
        {
            Application.Quit();
        }*/
        
    private void ResumeButtonClicked(ResumeButtonClickedEvent e)
    {
        Resume();
    }
        
    private void WorldSelectionButtonClicked(WorldSelectionButtonClickedEvent e)
    {
        GoBackToWorldSelection();
    }
        
    private void PlayButtonClicked(PlayButtonClickedEvent e)
    {
        Play();
    }

    #endregion

    #region GameState methods

    private void SaveOptions(float volume)
    {
        Debug.Log("volume to save : " + volume);
        PlayerPrefs.SetFloat(m_IdPlayer + ".OPTIONS.VOLUME", volume);
        PlayerPrefs.Save();
        float vol = PlayerPrefs.GetFloat(GameManager.Instance.m_IdPlayer + ".OPTIONS.VOLUME", 100);
        Debug.Log("volume sauvegardé : " + vol);
    }
    private void ShowCredits()
    {
        EventManager.Instance.Raise(new ShowCreditsEvent());
            
    }
        
    private void ScrollMenuPlayer()
    {
        //TODO: est ce qu'on peut changer le gameState comme ça avec une condition dessus
        if (m_GameState.Equals(GameState.gameScrollPlayerMenu))
        {
            EventManager.Instance.Raise(new GameScrollMenuPlayerButtonClickedEvent() {scrolled = true});
            m_GameState = GameState.gameWorldMenu;
        }
        else
        {
            EventManager.Instance.Raise(new GameScrollMenuPlayerButtonClickedEvent() {scrolled = false});
            m_GameState = GameState.gameScrollPlayerMenu;
        }
    }

    /// <summary>
    /// Add a player on the database and ask ChooseGame to start the world choice menu
    /// </summary>
    /// <param name="idPlayer">Player ID</param>
    /// <param name="namePlayer">Player name</param>
    private void PlayerCreate(int idPlayer, string namePlayer)
    {
        PlayerPrefs.SetString(idPlayer + ".NAME", namePlayer);
        PlayerPrefs.Save();
        WorldMenu();
    }

    /// <summary>
    /// Raise an event to display the player input ("Choose your name" UI) 
    /// </summary>
    private void PlayerCreateInput()
    {
        m_GameState = GameState.gameCreatePlayer;
        EventManager.Instance.Raise(new GamePlayerInputEvent());
        //TODO: Demander au prof si la fonction sert
    }

    /// <summary>
    /// Raise an event to display the world menu choice
    /// TODO: Correct name ?
    /// </summary>
    private void WorldMenu()
    {
        m_GameState = GameState.gameWorldMenu;
        EventManager.Instance.Raise(new GameWorldMenuEvent());
        //TODO: Load toutes les donnees concernant le joueur
    }

    /// <summary>
    /// Raise an event to display the player choice menu.
    /// 
    /// </summary>
    private void PlayerMenu()
    {
        SetTimeScale(1); // TODO: Remove ?

        m_GameState = GameState.gamePlayerMenu;
        //if(MusicLoopsManager.Instance)MusicLoopsManager.Instance.PlayMusic(Constants.MENU_MUSIC);
        EventManager.Instance.Raise(new GamePlayerMenuEvent());
        //TODO: Load les players pref des joueurs (name, temps de jeu)
    }
        
    private void Pause()
    {
        if (!IsPlaying) return;
        SetTimeScale(0);
        m_GameState = GameState.gamePause;
        EventManager.Instance.Raise(new GamePauseEvent());
    }
    private void Resume()
    {
        if (IsPlaying)
        {
            return;
        }
        SetTimeScale(1);
        m_GameState = GameState.gamePlay;
        EventManager.Instance.Raise(new GameResumeEvent());
    }

    private void GoBackToWorldSelection()
    {
        SetTimeScale(1);
        WorldMenu();
    }
        
    private void Play()
    {
        InitNewGame();
        SetTimeScale(1);
        m_GameState = GameState.gamePlay;
        EventManager.Instance.Raise(new GamePlayEvent());
    }
        
    /*private void Menu()
        {
            
        }*/

    #endregion
}