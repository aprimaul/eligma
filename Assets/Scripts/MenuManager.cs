using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Buttons;
using Common;
using CustomAnimation;
using SDD.Events;
using Tools;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : Manager<MenuManager>
{
    [Header("MenuManager")]

    #region Panels
    [Header("Panels")]
    [SerializeField] GameObject m_PanelMainMenu;
    [SerializeField] GameObject m_PanelBackgroundMenu;
    [SerializeField] GameObject m_superPanelMainScreens;
        
    //[SerializeField] GameObject m_PanelInGameMenu;
    //[SerializeField] GameObject m_PanelGameOver;
    [SerializeField] GameObject m_PanelPlayerMenu;
    [SerializeField] GameObject m_PanelWorldMenu;
    [SerializeField] GameObject m_PanelCreatePlayerSubMenu;
    [SerializeField] GameObject m_PanelScrollingPlayerMenu;
    [SerializeField] private GameObject m_PanelPopUpQuitGame;
    [SerializeField] GameObject m_PanelPauseGameMenu;
    [SerializeField] GameObject m_PanelWinGameMenu;
    [SerializeField] GameObject m_PanelLooseGameMenu;
    [SerializeField] GameObject m_PanelCredits;
    [SerializeField] GameObject m_PanelOptions;
    [SerializeField] GameObject m_PanelRecords;
    [SerializeField] Text m_PanelRecordsFire;
    [SerializeField] Text m_PanelRecordsAir;
    [SerializeField] Text m_PanelRecordsGround;
    [SerializeField] Text m_PanelRecordsWatter;

    List<GameObject> m_AllPanels;

    #endregion

    #region Events' subscription

    public override void SubscribeEvents()
    {
        base.SubscribeEvents();

        // GameManager
        EventManager.Instance.AddListener<GamePlayerInputEvent>(GamePlayerInputMenu);
        EventManager.Instance.AddListener<GamePlayerMenuEvent>(GamePlayerMenu);
        EventManager.Instance.AddListener<GameWorldMenuEvent>(GameWorldMenu);
        EventManager.Instance.AddListener<GameScrollMenuPlayerButtonClickedEvent>(ScrollMenuPlayer);
        EventManager.Instance.AddListener<GameResumeEvent>(GameResume);
        EventManager.Instance.AddListener<GamePauseEvent>(GamePause);
        EventManager.Instance.AddListener<ShowCreditsEvent>(ShowCredits);
    }

    public override void UnsubscribeEvents()
    {
        base.UnsubscribeEvents();

        // GameManager
        EventManager.Instance.RemoveListener<GamePlayerInputEvent>(GamePlayerInputMenu);
        EventManager.Instance.RemoveListener<GamePlayerMenuEvent>(GamePlayerMenu);
        EventManager.Instance.RemoveListener<GameWorldMenuEvent>(GameWorldMenu);
        EventManager.Instance.RemoveListener<GameScrollMenuPlayerButtonClickedEvent>(ScrollMenuPlayer);
        EventManager.Instance.RemoveListener<GameResumeEvent>(GameResume);
        EventManager.Instance.RemoveListener<GamePauseEvent>(GamePause);
        EventManager.Instance.AddListener<ShowCreditsEvent>(ShowCredits);
    }

    #endregion

    #region Manager implementation

    protected override IEnumerator InitCoroutine()
    {
        yield break;
    }

    #endregion

    #region Monobehaviour lifecycle

    protected override void Awake()
    {
        base.Awake();
        RegisterPanels();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            EscapeButtonHasBeenClicked();
        }
    }

    #endregion

    #region Panel Methods

    void RegisterPanels()
    {
        m_AllPanels = new List<GameObject>
        {
            m_PanelMainMenu,
            m_PanelPlayerMenu,
            m_PanelWorldMenu,
            m_PanelCreatePlayerSubMenu,
            m_PanelPauseGameMenu,
            m_PanelWinGameMenu,
            m_PanelLooseGameMenu
        };
    }

    void OpenPanel(GameObject panel)
    {
        foreach (var item in m_AllPanels.Where(item => item))
            item.SetActive(item == panel);
    }

    #endregion

    #region UI OnClick Events

    public void RecordsButtonHasBeenClicked()
    {
        m_PanelRecords.SetActive(true);
        AddBestScore(Constants.FIRE, m_PanelRecordsFire);
        AddBestScore(Constants.AIR, m_PanelRecordsAir);
        AddBestScore(Constants.GROUND, m_PanelRecordsGround);
        AddBestScore(Constants.WATER, m_PanelRecordsWatter);
    }

    private void AddBestScore(string world, Text textPanel)
    {
        float recFire = PlayerPrefs.GetFloat(GameManager.Instance.m_IdPlayer + ".BESTSCORE." + world, 0);
        textPanel.text = recFire == 0 ? "-" : TimeFormatterTools.FromSecondsToTime(recFire);
    }
        
    public void TryAgainButtonHasBeenClicked()
    {
        //m_superPanelMainScreens.SetActive(false);
        EventManager.Instance.Raise(new TryAgainGameEvent());
    }
    public void SlideVolumeHasBeenChanged()
    {
        EventManager.Instance.Raise(new UpdateSoundVolume(){volume = m_PanelOptions.GetComponentInChildren<Slider>().value});
    }
        
    public void OptionButtonHasBeenClicked()
    {
        m_PanelOptions.SetActive(true);
        float vol = PlayerPrefs.GetFloat(GameManager.Instance.m_IdPlayer + ".OPTIONS.VOLUME", 100);
        m_PanelOptions.GetComponentInChildren<Slider>().value = vol;
        Debug.Log("option open set volume at : " + vol);
    }
        
    public void QuitOptionHasBeenClicked()
    {
        m_PanelOptions.SetActive(false);
        EventManager.Instance.Raise(new QuitOptionClickedEvent(){volume = m_PanelOptions.GetComponentInChildren<Slider>().value});
    }
    public void CreditButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new CreditsButtonClickedEvent());

    }
    public void ScrollMenuPlayerButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new ScrollMenuPlayerButtonClickedEvent());
    }

    /// <summary>
    /// On click "OK" button on the "Choose your name" Sub UI 
    /// </summary>
    public void CreatePlayerButtonHasBeenClicked()
    {
        Text[] textContents = m_PanelCreatePlayerSubMenu.GetComponentInChildren<InputField>()
            .GetComponentsInChildren<Text>();
        EventManager.Instance.Raise(textContents.Length > 1
            ? new CreatePlayerButtonClickedEvent() {eNamePlayer = textContents[1].text}
            : new CreatePlayerButtonClickedEvent() {eNamePlayer = ""});
    }


    /// <summary>
    /// OnClick on a player button
    /// </summary>
    /// <param name="button"></param>
    public void ChoosePlayerButtonHasBeenClicked(Button button)
    {
        EventManager.Instance.Raise(new ChoosePlayerButtonClickedEvent()
        {
            eCreatePlayer = button.GetComponentInChildren<Text>().text.Equals("New game"),
            eIdButton = button.GetComponentInChildren<PlayerGameButtonBehavior>().m_Id
        });
    }

    /// <summary>
    /// OnClick Escape button
    /// </summary>
    private void EscapeButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new EscapeButtonClickedEvent());
    }
        
    /// <summary>
    /// OnClick Resume button on pause panel
    /// </summary>
    public void ResumeButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new ResumeButtonClickedEvent());
    }

    /// <summary>
    /// OnClick World selection button from the Pause panel
    /// </summary>
    public void WorldSelectionFromPauseHasBeenClicked()
    {
        //m_PanelPauseGameMenu.SetActive(false);
        //m_superPanelMainScreens.SetActive(true);
        EventManager.Instance.Raise(new WorldSelectionButtonClickedEvent());
    }

    /// <summary>
    /// OnClick MainMenu
    /// </summary>
    public void MainMenuButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new MainMenuButtonClickedEvent());
    }
		
    public void QuitGameButtonHasBeenClicked()
    {
        m_PanelPopUpQuitGame.SetActive(true);
    }
		
    public void CancelQuitGameButtonHasBeenClicked()
    {
        m_PanelPopUpQuitGame.SetActive(false);
    }
		
    public void ConfirmQuitGameButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new ConfirmQuitGameButtonClickedEvent());
    }

        
    // Review: Pour plus tard
    public void PlayButtonHasBeenClicked(Button b)
    {
        string idWorld = b.tag;
        EventManager.Instance.Raise(new PlayButtonClickedEvent(){tag = idWorld});
    } 
        
    // Review: Pour plus tard
         
    public void QuitButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new QuitButtonClickedEvent());
    }

    #endregion

    #region Callbacks to GameManager events

    private void ShowCredits(ShowCreditsEvent e)
    {
        string credits = ReadFileTools.ReadFile(Constants.CREDITS_PATH);
        m_PanelCredits.GetComponentInChildren<Text>().text = credits;
        m_PanelCredits.GetComponentInChildren<TextPanelAnimation>().ScrollToTopUntilTheEnd();
    }
    private void ScrollMenuPlayer(GameScrollMenuPlayerButtonClickedEvent e)
    {
        GameObject go = m_PanelScrollingPlayerMenu.GetComponentInChildren<MenuPanelAnimation>().m_animationGameObject;
        Vector3 posStart = go.transform.position;
        if (!e.scrolled)
        {
            m_PanelScrollingPlayerMenu.GetComponentInChildren<MenuPanelAnimation>()
                .ScrollButton(1f, new Vector3(posStart.x, 555), EasingFunction.EaseInOutExpo);
            m_PanelScrollingPlayerMenu.GetComponentInChildren<Image>().maskable = true;
            m_PanelScrollingPlayerMenu.GetComponentInChildren<Image>().raycastTarget = true;
        }
        else
        {
            m_PanelScrollingPlayerMenu.GetComponentInChildren<MenuPanelAnimation>()
                .ScrollButton(1f, new Vector3(posStart.x, 1430), EasingFunction.EaseOutExpo);
            m_PanelScrollingPlayerMenu.GetComponentInChildren<Image>().maskable = false;
            m_PanelScrollingPlayerMenu.GetComponentInChildren<Image>().raycastTarget = false;

        }
    }

    /// <summary>
    /// Open the PanelWorldMenu only
    /// </summary>
    /// <param name="e"></param>
    protected void GameWorldMenu(GameWorldMenuEvent e)
    {
        OpenPanel(m_PanelWorldMenu);
        m_PanelBackgroundMenu.SetActive(true);
        m_PanelScrollingPlayerMenu.GetComponentInChildren<Text>().text =
            PlayerPrefs.GetString(GameManager.Instance.m_IdPlayer + ".NAME");
        float vol = PlayerPrefs.GetFloat(GameManager.Instance.m_IdPlayer + ".OPTIONS.VOLUME", 100);
        EventManager.Instance.Raise(new UpdateSoundVolume(){volume = vol});
    }

    /// <summary>
    /// Set PanelCreatePlayerSubMenu active
    /// </summary>
    /// <param name="e"></param>
    protected void GamePlayerInputMenu(GamePlayerInputEvent e)
    {
        m_PanelCreatePlayerSubMenu.SetActive(true);
    }

    /// <summary>
    /// Open the PlayerPanelMenu only
    /// </summary>
    /// <param name="e"></param>
    protected void GamePlayerMenu(GamePlayerMenuEvent e)
    {
        OpenPanel(m_PanelPlayerMenu);
    }

    /// <summary>
    /// TODO: Load level + maps ?
    /// </summary>
    /// <param name="e"></param>
    protected override void GamePlay(GamePlayEvent e)
    {
        //OpenPanel(null);
        m_superPanelMainScreens.SetActive(false);
        //m_PanelBackgroundMenu.SetActive(false);
    }

    /// <summary>
    /// TODO: Open PanelPause + stop timer
    /// </summary>
    /// <param name="e"></param>
    protected override void GamePause(GamePauseEvent e)
    {
        m_superPanelMainScreens.SetActive(true);
        OpenPanel(m_PanelPauseGameMenu);
            
    }

    /// <summary>
    /// TODO: Hide all panels + restart timer
    /// </summary>
    /// <param name="e"></param>
    protected override void GameResume(GameResumeEvent e)
    {
        m_superPanelMainScreens.SetActive(false);
        //m_PanelPauseGameMenu.SetActive(false);
    }

    /// <summary>
    /// TODO: Show world choice panel + stop timer + add time + reset timer
    /// </summary>
    /// <param name="e"></param>
    protected override void GameOver(GameOverEvent e)
    {
        m_superPanelMainScreens.SetActive(true);
        OpenPanel(m_PanelLooseGameMenu);
    }
    /// <summary>
    /// TODO: Show world choice panel + stop timer + add time + reset timer
    /// </summary>
    /// <param name="e"></param>
    protected override void GameVictory(GameVictoryEvent e)
    {
        Debug.Log("display panel win menu");
        m_superPanelMainScreens.SetActive(true);
        OpenPanel(m_PanelWinGameMenu);
        //m_PanelWinGameMenu.SetActive(true);
    }

    #endregion
}