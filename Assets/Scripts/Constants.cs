﻿using UnityEngine;

public static class Constants
{
    //Musics
    public static int GAMEPLAY_MUSIC = 0;
    public static int MENU_MUSIC = 1;
    public static int FIREWORLD_MUSIC = 3;
    public static int AIRWORLD_MUSIC = 4;
    public static int GROUNDWORLD_MUSIC = 5;
    public static int WATERWORLD_MUSIC = 6;

    //Sfx
    public static string GAMEOVER_SFX = "GameOver";
    public static string GAMEVICTORY_SFX = "GameVictory";
    
    //credits
    public static string CREDITS_PATH = "Text/Credits";
    
    //Name level
    public static string FIRE = "FIRE";
    public static string AIR = "AIR";
    public static string GROUND = "GROUND";
    public static string WATER = "WATTER";
    
    //Scene level
    public static string FIRESCENE = "PetiteMap2";
    public static string AIRSCENE = "AirLevel";
    public static string GROUNDSCENE = "EarthLevel";
    public static string WATERSCENE = "WaterLevel";
    
}