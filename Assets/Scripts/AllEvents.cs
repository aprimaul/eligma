﻿using System.Collections;
using System.Collections.Generic;
using Common;
using GameEntity;
using UnityEngine;
using SDD.Events;
using Tools;
using UnityEngine.Rendering;

#region GameManager Events

public class TryAgainGameEvent : SDD.Events.Event
{
    
}
public class ShowCreditsEvent : SDD.Events.Event
{
    
}
public class ScrollMenuPlayerButtonClickedEvent : SDD.Events.Event
{
}

public class GameMenuEvent : SDD.Events.Event
{
}

/// <summary>
/// GameManager -> XXX
/// TODO: UNUSED
/// </summary>
public class GamePlayEvent : SDD.Events.Event
{
}

/// <summary>
/// GameManager -> XXX
/// TODO: UNUSED
/// </summary>
public class GamePauseEvent : SDD.Events.Event
{
}

/// <summary>
/// GameManager -> XXX
/// TODO: UNUSED
/// </summary>
public class GameResumeEvent : SDD.Events.Event
{
}

/// <summary>
/// GameManager -> XXX
/// TODO: UNUSED
/// </summary>
public class GameOverEvent : SDD.Events.Event
{
}

/// <summary>
/// GameManager -> XXX
/// TODO: UNUSED
/// </summary>
public class GameVictoryEvent : SDD.Events.Event
{
}

/// <summary>
/// GameManager -> XXX
/// TODO: UNUSED
/// </summary>
public class GameStatisticsChangedEvent : SDD.Events.Event
{
    public float eBestScore { get; set; }
    public float eScore { get; set; }
    public int eNLives { get; set; }
}

/// <summary>
/// GameManager -> MenuManager
/// To display player choice menu
/// </summary>
public class GamePlayerMenuEvent : SDD.Events.Event
{
}

/// <summary>
/// GameManager -> MenuManager
/// To display player choice menu
/// </summary>
public class GamePlayerInputEvent : SDD.Events.Event
{
}

/// <summary>
/// GameManager -> MenuManager
/// To display world choice menu
/// </summary>
public class GameWorldMenuEvent : SDD.Events.Event
{
}

#endregion

#region MenuManager Events

public class QuitOptionClickedEvent : SDD.Events.Event
{
    public float volume { get; set; }
}
public class CreditsButtonClickedEvent : SDD.Events.Event
{
    
}

public class GameScrollMenuPlayerButtonClickedEvent : SDD.Events.Event
{
    public bool scrolled { get; set; }
}
public class GameCreatePlayerEvent : SDD.Events.Event
{
}

public class GamePlayerCreationEvent : SDD.Events.Event
{
}

/// <summary>
/// MenuManager -> GameManager
/// OnClic "OK" button on create player panel
/// </summary>
public class CreatePlayerButtonClickedEvent : SDD.Events.Event
{
    public string eNamePlayer { get; set; }
}

/// <summary>
/// MenuManager -> GameManager
/// OnClic on any button (UP, RIGHT, LEFT, BOTTOM) on choose player menu
/// </summary>
public class ChoosePlayerButtonClickedEvent : SDD.Events.Event
{
    public bool eCreatePlayer { get; set; }
    public int eIdButton { get; set; }
}

/// <summary>
/// MenuManager -> GameManager
/// OnClic on the first panel (welcome panel)
/// </summary>
public class MainMenuButtonClickedEvent : SDD.Events.Event
{
    public bool createPlayer { get; set; }
    public int idButton { get; set; }
}

/*public class QuitGameButtonClickedEvent : SDD.Events.Event
{ }*/

public class ConfirmQuitGameButtonClickedEvent : SDD.Events.Event
{
    
}

public class EscapeButtonClickedEvent : SDD.Events.Event
{
}

public class ResumeButtonClickedEvent : SDD.Events.Event
{
}


public class WorldSelectionButtonClickedEvent : SDD.Events.Event{}

 public class PlayButtonClickedEvent : SDD.Events.Event
 {
     public string tag { get; set; }
 }

public class QuitButtonClickedEvent : SDD.Events.Event
{ }

/*

#endregion

#region Score Event

/*public class ScoreItemEvent : SDD.Events.Event
{
    public float eScore;
}*/

#endregion

#region Board manager event

public class FreeDirectionEvent : SDD.Events.Event
{
    public Entity EntityPlayMode { get; set; }

    public CustomVector3[] directions { get; set; }
}

#endregion

#region Entity Event

#region Move event
public class AskFreeDirectionsEvent : SDD.Events.Event
{
    public Entity eEntityPlayMode { get; set; }
    public Vector3Int[] eDirections { get; set; }
}

public class OnMoveEntityEvent : SDD.Events.Event
{
    public Vector3Int eFrom { get; set; }
    public Vector3Int eTo { get; set; }
}
#endregion

#region Spell events

public class LaunchSpell : SDD.Events.Event
{
    public Entity eEntity { get; set; }
    public Spell eSpell { get; set; }
    public bool hitArray { get; set; }
    public int spellArrayIndex { get; set; }
}

public class LaunchBestSpell : SDD.Events.Event
{
    public Entity eEntity { get; set; }
    public Spell[] eSpells { get; set; }
    public bool bestHitSpell = true; // if false, best heal spell
}


#endregion

#region Health events

public class EntityDeathEvent : SDD.Events.Event
{
    public Entity entity { get; set; }
}

#endregion
#endregion

#region Spell manager event

public class SpellPreLaunchEvent : SDD.Events.Event
{
    public SpellLaunchEvent eSpellLaunchEvent { get; set; }
    public Vector3Int[] ePositions { get; set; }
    public Entity eEntity { get; set; }
    public string eButtonString { get; set; }
}

public class SpellLaunchEvent : SDD.Events.Event
{
    public Entity eEntity { get; set; }
    public Spell eSpell { get; set; }
    public Vector3Int[] ePositions { get; set; }
    public bool hitArray { get; set; }
    public int spellArrayIndex { get; set; }
}


#endregion

#region HudManagerEvent

public class HudManagerReadyEvent : SDD.Events.Event
{
    
}

public class AddSpellEvent : SDD.Events.Event
{
    public bool hit { get; set; }
    public Spell spell { get; set; }
}

public class DeleteSpellEvent : SDD.Events.Event
{
    public Spell spell { get; set; }
}

public class UpdateHealth : SDD.Events.Event
{
    public float newHealth { get; set; }
    public float maxHealth { get; set; }
}

#endregion

#region MusicLoopsManagers

public class UpdateSoundVolume : SDD.Events.Event
{
    public float volume { get; set; }
}

#endregion