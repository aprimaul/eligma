﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace CustomAnimation
{
    public class TextPanelAnimation : MonoBehaviour
    {
        [FormerlySerializedAs("panel")] public GameObject m_Panel;

        private Text m_TextObject;
        private int m_Height;
        
        // Start is called before the first frame update
        void Start()
        {
            m_TextObject = m_Panel.GetComponentInChildren<Text>();
            m_Height = Convert.ToInt32(m_Panel.GetComponent<RectTransform>().rect.height);
        }
        
        public void ScrollToTopUntilTheEnd()
        {
            m_Panel.SetActive(true);
            StartCoroutine(ScrollToTopUntilTheEndCoroutine());
        }

        private IEnumerator ScrollToTopUntilTheEndCoroutine()
        {
            Vector2 verticalIncrement = new Vector2(0,5);
            Vector2 posStart = m_TextObject.transform.position;
            Vector2 posEnd = new Vector2(0,m_TextObject.preferredHeight);
            Vector2 posFromBotton = posStart + posEnd;
            Vector2 posCourante = posStart;
            while (posFromBotton.y > -m_Height-100)
            {
                Transform obTransform = m_TextObject.transform;
                obTransform.position= Vector2.Lerp(posCourante, posCourante + verticalIncrement,1);
                posCourante = obTransform.position;
                posFromBotton -= verticalIncrement;
                yield return null;
            }
            m_TextObject.transform.position = posStart;
            m_Panel.SetActive(false);
        }
    }
}