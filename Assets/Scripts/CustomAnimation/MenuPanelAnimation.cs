﻿using System.Collections;
using Tools;
using UnityEngine;
using UnityEngine.Serialization;

namespace CustomAnimation
{
    public class MenuPanelAnimation : MonoBehaviour
    {
        [FormerlySerializedAs("go")] public GameObject m_animationGameObject;

        private IEnumerator ScrollingAnimation(float duration, Vector3 posTarget, EasingDelegate anim)
        {
            Vector3 posStart = m_animationGameObject.transform.position;
            float elapsedTime = 0;
            //Vector3 posTarget = new Vector3(posStart.x, 555);
            while (elapsedTime < duration)
            {
                float k = elapsedTime / duration;
                m_animationGameObject.transform.position = Vector3.Lerp(posStart, posTarget, anim(0, 1, k));
                elapsedTime += Time.deltaTime;
                yield return null;
            }

            m_animationGameObject.transform.position = posTarget;
        }

        public void ScrollButton(float duration, Vector3 posTarget, EasingDelegate anim)
        {
            StartCoroutine(ScrollingAnimation(duration, posTarget, anim));
        }
    }
}