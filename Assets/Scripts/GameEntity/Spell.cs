﻿using System;
using Common;
using SDD.Events;
using UnityEngine;

namespace GameEntity
{
    [Serializable]
    public sealed class Spell : MonoBehaviour
    {
        [Header("Global information")]
        [SerializeField] public string m_SpellName;
        [SerializeField] public bool m_NeedFieldOfView;
        [BitMask(typeof(Direction))] public Direction m_SpellDirections;
        [SerializeField] public int m_RangeStart = 1;
        [SerializeField] public int m_RangeEnd;
        [SerializeField] public int m_SpellUsageCount = 100000;
        [SerializeField] public Sprite m_SpellSprite;

        [Header("Direct damages and heal")] 
        [SerializeField] public bool m_DirectDamagesAndHealEnabled;
        [SerializeField] public int m_PhysicalDamages;
        [SerializeField] public int m_HealPoints;

        [Header("Poison")] 
        [SerializeField] public bool m_PoisonEnabled;
        [SerializeField] public float m_PoisonDuration;
        [SerializeField] public int m_PoisonDamagesPerHit;
        [SerializeField] public float m_PoisonHitEach;

        public bool UseAllowed()
        {
            m_SpellUsageCount--;
            if (m_SpellUsageCount != 0) return true;
            
            Debug.Log("Adios "+m_SpellName);
            EventManager.Instance.Raise(new DeleteSpellEvent(){spell = this});
            Destroy(gameObject);
            Destroy(this);
            return false;
        }
    }
}