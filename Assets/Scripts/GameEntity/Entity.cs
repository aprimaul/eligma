using System;
using System.Collections;
using System.Linq;
using Common;
using JetBrains.Annotations;
using SDD.Events;
using Tools;
using UnityEngine;
using Debug = UnityEngine.Debug;
using IEventHandler = SDD.Events.IEventHandler;
using Random = System.Random;

namespace GameEntity
{
    public sealed class Entity : MonoBehaviour, IEventHandler
    {
        #region Entity variables

        #region Editor editable characteristics
        [Header("Global characteristics")] 
        [SerializeField] public string m_EntityName;
        [SerializeField] public GameObject m_Entity;
        [SerializeField] public Team m_Team = Team.Red;
        [SerializeField] private bool m_IsInvulnerable;
        [SerializeField] private int m_HealthPoints;
        [SerializeField] private bool m_IsAi = true;
        [SerializeField] public bool m_IsInvisible;

        [Header("Move characteristics")] 
        [BitMask(typeof(Direction))] public Direction m_MoveDirections;
        [SerializeField] private bool m_IsMotionless = true;
        [SerializeField] private float m_MoveSpeed = 2f;
        

        [Header("IA definition (IA ONLY)")] 
        [SerializeField] private float m_WaitThinkingDuration = .5f;

        [Header("IA Triggers (IA ONLY)")] 
        [SerializeField] public bool m_IsPushable;
        [SerializeField] public bool m_IsRetrievable;
        [SerializeField] public Spell m_RetrievableSpell;
        
        [Header("Spells")]
        [SerializeField] private bool m_IsSpellLess = true;
        [SerializeField] private Spell[] m_HitSpells = new Spell[4];
        [SerializeField] private Spell[] m_HealSpells = new Spell[4];

        #endregion
        
        #region Spells variables
        private Action d_SpellCheck;
        private SpellChoiceBehaviorDelegate d_SpellChoiceBehavior;

        private bool m_HasLaunchedSpell;

        private int[] m_HitSpellsPresent;
        private int[] m_HealSpellsPresent;
        private int[] m_OnTriggerSpellPresent;
        
        #endregion

        #region Move variables
        private Action d_MoveCheck;
        private MoveChoiceBehaviorDelegate d_MoveChoiceBehavior;
        
        private bool m_CanMove;
        private Vector2 m_MoveVector = new Vector2(0, 0);
        #endregion
        
        #region Other variables
        private RectTransform m_SpriteRect;
        private GameObject m_HealthBarPrefab;
        private int m_maxHealth;
        private HealthBar m_healthBar;
        public bool m_IsReady { get; private set; }
        public Direction m_dir { get; private set; }
        private Random m_Random = new Random();
        private Animator m_charAnimator;
        public Vector3Int MatrixPosition { get; set; }
        #endregion

#endregion

#region MonoBehaviour lifecycle
        private void Awake()
        {
            SubscribeEvents();
        }

        public Vector2 GetGeometricPosition()
        {
            return m_SpriteRect.position;
        }

        private void Start()
        {
            m_SpriteRect = GetComponent<RectTransform>();
            m_charAnimator = m_Entity.GetComponent<Animator>();
            //m_Sprite = GetComponent<SpriteRenderer>();
            //TODO: A tester
            if(m_IsInvisible)
                m_Entity.GetComponent<Renderer>().enabled = false;
            m_maxHealth = m_HealthPoints;
            DefineChecks();
            CreateSpellsIndexes();
            AddHealthBar();
            
            m_IsReady = true;
            StartCoroutine(ThinkingCoroutine());
        }

        public void OnDestroy()
        {
            UnsubscribeEvents();
            //BoardManager.Instance.BoardMatrix.SetValue(MatrixPosition, 1);
            EventManager.Instance.Raise(new EntityDeathEvent(){entity = this});
            Destroy(gameObject);
            Destroy(m_HealthBarPrefab);
        }

        private void FixedUpdate()
        {
            if (m_CanMove)
            {
                Vector3 v = m_SpriteRect.position + new Vector3(m_MoveVector.x, m_MoveVector.y) * Time.fixedDeltaTime;
                v.z = -9;
                m_HealthBarPrefab.transform.position = v;
            }
        }

#endregion

#region Event subscribe methods

        public void SubscribeEvents()
        {
            EventManager.Instance.AddListener<FreeDirectionEvent>(ChooseMoveDirection);
            EventManager.Instance.AddListener<SpellLaunchEvent>(SpellReceived);
            EventManager.Instance.AddListener<HudManagerReadyEvent>(CallHudManager);
        }

        public void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<FreeDirectionEvent>(ChooseMoveDirection);
            EventManager.Instance.RemoveListener<SpellLaunchEvent>(SpellReceived);
            EventManager.Instance.RemoveListener<HudManagerReadyEvent>(CallHudManager);
        }

        private void CallHudManager(HudManagerReadyEvent e)
        {
            StartCoroutine(InitHudManager());
        }

        #endregion

#region Start functions

        private void AddHealthBar()
        {
            var position = m_SpriteRect.transform.position;
            position.z = -9;
            m_HealthBarPrefab = Instantiate(Resources.Load("Prefabs/HealthBar")) as GameObject;
            if (m_HealthBarPrefab != null)
            {
                m_HealthBarPrefab.transform.position = position;
                if (m_HealthBarPrefab == null) return;
                m_healthBar = m_HealthBarPrefab.GetComponentInChildren<HealthBar>();
            }
            m_maxHealth = m_HealthPoints;
            m_healthBar.SetMaxHealth(m_maxHealth);
            m_healthBar.SetHealth(m_HealthPoints);
        }

        private void DefineChecks()
        {
            if (m_IsAi)
            {
                var cr = new WaitForSeconds(4);
                                                
                d_MoveCheck = AutomaticMoveCheck;
                d_MoveChoiceBehavior = RandomMoveDirection;
                m_WaitThinkingDuration = 0.5f;
                d_SpellCheck = AutomaticSpellCheck;
                
            }
            else
            {
                d_MoveCheck = ControllableMoveCheck;
                d_MoveChoiceBehavior = GetFirstMoveDirection;
                m_WaitThinkingDuration = 0;

                d_SpellCheck = ControllableSpellCheck;
            }
        }

        private void CreateSpellsIndexes()
        {
            m_HealSpellsPresent = new int[100];
            m_HitSpellsPresent = new int[100];
            //m_OnMoveSpellsPresent = new int[100];
            
            for (int i = 0; i < m_HitSpells.Length; i++)
                m_HitSpellsPresent[i] = m_HitSpells[i] == null ? 0 : 1;
            
            for (int i = 0; i < m_HealSpells.Length; i++)
                m_HealSpellsPresent[i] = m_HealSpells[i] == null ? 0 : 1;
            
            /*for (int i = 0; i < m_OnMoveSpells.Length; i++)
                m_OnMoveSpellsPresent[i] = m_OnMoveSpells[i] == null ? 0 : 1;*/
        }

        private IEnumerator InitHudManager()
        {
            yield return new WaitForSeconds(0.1f);
            if (m_Team.Equals(Team.Blue))
            {
                foreach (var sp in m_HitSpells)
                {
                    if(sp != null)
                        EventManager.Instance.Raise(new AddSpellEvent(){hit=true, spell = sp});
                }
                foreach (var sp in m_HealSpells)
                {
                    if(sp != null)
                        EventManager.Instance.Raise(new AddSpellEvent(){hit=false, spell = sp});
                }
                EventManager.Instance.Raise(new UpdateHealth(){newHealth = m_HealthPoints, maxHealth = m_maxHealth});
            }
        }

        #endregion
        
        
        bool isCheckMove = true;
        private static readonly int Condition = Animator.StringToHash("condition");

        #region Entity Coroutines
        private IEnumerator ThinkingCoroutine()
        {
            for (;;)
            {
                if (!(m_HasLaunchedSpell || m_CanMove))
                {
                    if (m_IsAi)
                    {
                        if (isCheckMove)
                        {
                            d_MoveCheck?.Invoke();
                        }
                        else
                        {
                            d_SpellCheck?.Invoke();
                        }
                    
                        isCheckMove = !isCheckMove;
                    }
                    else
                    {
                        d_MoveCheck?.Invoke();
                        d_SpellCheck?.Invoke();
                    }
                }

                if (m_IsAi)
                    yield return new WaitForSeconds(m_WaitThinkingDuration);
                else
                    yield return null;
            }
        }

#endregion
        
#region Move functions

        #region Callback to BoardManager move events
        private void ChooseMoveDirection(FreeDirectionEvent e)
        {
            if (e.EntityPlayMode != this)
                return;

            var direction = d_MoveChoiceBehavior(e);
            Vector3Int v = direction.ToMapVector3();
            m_dir = DirectionTools.Vector3IntToDirection(v);

            Vector3Int newPosition = MatrixPosition + direction.ToMapVector3();

            EventManager.Instance.Raise(new OnMoveEntityEvent() {eFrom = Vector3Int.CeilToInt(MatrixPosition), eTo = Vector3Int.CeilToInt(newPosition)});
            
            MatrixPosition = newPosition;
            
            m_MoveVector = direction.ToGeometricVector3() * m_MoveSpeed;

            StartCoroutine(MoveCoroutine());
        }
        #endregion

        #region Coroutines
        private IEnumerator MoveCoroutine()
        {
            m_Entity.transform.rotation = Quaternion.Euler(DirectionTools.GetRotationFromDirection(m_dir));
            m_charAnimator.SetInteger(Condition,1);
            
            m_CanMove = true;
            float elapsedTime = 0;
            
            float duration = 1.000000f / m_MoveSpeed;

            int layerDirectionMove = 0;

            if (m_MoveVector.x >= 0 && m_MoveVector.y >= 0 && m_MoveVector.x + m_MoveVector.y > 0)
            {
                layerDirectionMove += 5;
            } else if (m_MoveVector.x <= 0 && m_MoveVector.y <= 0 && m_MoveVector.x + m_MoveVector.y < 0)
            {
                layerDirectionMove += -5;
            }
            else if(Math.Abs(Math.Abs(m_MoveVector.x) - 2) < 0.01)
            {
                layerDirectionMove = m_MoveVector.y < 0 ? 5 : -5;
            }

            while (elapsedTime < duration*0.4)
            {
                elapsedTime += Time.deltaTime;
                Vector2 move = m_MoveVector * (Time.deltaTime / duration / m_MoveSpeed);
                m_SpriteRect.transform.position += new Vector3(move.x, move.y);
                yield return null; // attend la prochaine frame
            }
            
            m_SpriteRect.transform.position += new Vector3(0,0, Convert.ToInt32(layerDirectionMove));

            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                Vector2 move = m_MoveVector * (Time.deltaTime / duration / m_MoveSpeed);
                m_SpriteRect.transform.position += new Vector3(move.x, move.y);
                yield return null; // attend la prochaine frame
            }
            
            var position = m_SpriteRect.transform.position;
            
            var newPos =
                CustomVector3.FromMatrixVector3(MatrixPosition).ToGeometricVector3();
            newPos.x += 0.500000f;
            newPos.z = position.z;
            
            m_SpriteRect.transform.position = newPos;

            m_MoveVector = new Vector2(0, 0);
            m_dir = DirectionTools.GetStaticDirectionFrom(m_dir);
            m_Entity.transform.rotation = Quaternion.Euler(DirectionTools.GetRotationFromDirection(m_dir));
            m_charAnimator.SetInteger(Condition,0);
            m_CanMove = false;

            yield return null;
        }
        #endregion

        #region Move check delegate
        private void ControllableMoveCheck()
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            // Evite un déplacement en cas de blocage du joystick de la manette dans sa position proche de 0
            int x = horizontal < -0.09 ? 1 : horizontal > 0.09 ? -1 : 0;
            int y = vertical < -0.09 ? -1 : vertical > 0.09 ? 1 : 0;

            if (x == 0 && y == 0)
                return;

            Vector3Int direction = new Vector3Int(x, y, 0);

            EventManager.Instance.Raise(new AskFreeDirectionsEvent()
                {eEntityPlayMode = this, eDirections = new[] {direction}});
        }
        
        private void AutomaticMoveCheck()
        {            
            EventManager.Instance.Raise(new AskFreeDirectionsEvent()
               {eEntityPlayMode = this, eDirections = DirectionTools.AllDirectionsAI});
        }
        #endregion

        #region Move choice behaviors

        private CustomVector3 GetFirstMoveDirection(FreeDirectionEvent e)
        {
            return e.directions[0];
        }

        private CustomVector3 RandomMoveDirection(FreeDirectionEvent e)
        {
            int len = e.directions.Length-1;
            int random = m_Random.Next(0, len);
            return e.directions[random];
        }
        #endregion

#endregion

#region Spells functions

        #region Callback to SpellManager spells event

        private void SpellReceived(SpellLaunchEvent e)
        {
            StartCoroutine(AttackAnimationCoroutine(e));
        }

        #endregion

        #region Coroutines

        private IEnumerator DamageAnimationCoroutine(SpellLaunchEvent e)
        {
            if(e.hitArray)
                m_charAnimator.SetInteger(Condition,3);
            
            yield return new WaitForSeconds(1.5f);
            m_HealthPoints -= e.eSpell.m_PhysicalDamages;
            if(m_Team.Equals(Team.Blue))
                EventManager.Instance.Raise(new UpdateHealth(){newHealth = m_HealthPoints, maxHealth = m_maxHealth});
            m_healthBar.SetHealth(m_HealthPoints);
            Debug.Log(m_EntityName+" has lost "+e.eSpell.m_PhysicalDamages+"HP and now have "+m_HealthPoints);

            if (m_HealthPoints <= 0)
            {
                m_charAnimator.SetInteger(Condition,4);
                yield return new WaitForSeconds(1.5f);
                Destroy(this);
            }
            m_charAnimator.SetInteger(Condition,0);
        }
        
        private IEnumerator AttackAnimationCoroutine(SpellLaunchEvent e)
        {
            
           if (e.eEntity == this)
           {
               m_charAnimator.SetInteger(Condition,2);
               yield return new WaitForSeconds(1.5f);
               m_charAnimator.SetInteger(Condition,0);
               m_HasLaunchedSpell = false;
               if (!e.eSpell.UseAllowed())
               {
                   if (e.hitArray)
                       m_HitSpellsPresent[e.spellArrayIndex] = 0;
                   else
                       m_HealSpellsPresent[e.spellArrayIndex] = 0;
               }
           }
           else
           {
               yield return new WaitForSeconds(0.25f);
           }
           
           if(!e.ePositions.Contains(MatrixPosition))
               yield break;
            
           if(e.eEntity.m_Team == m_Team && e.eSpell.m_PhysicalDamages > 0)
               yield break;
           //Debug.Log("Spell "+e.eSpell.m_SpellName+" received on "+m_EntityName);
           StartCoroutine(DamageAnimationCoroutine(e));
        }

        #endregion

        #region Spell check delegate

        private void ControllableSpellCheck()
        {
            Spell[] array = m_HitSpells;
            bool spellFound = false;
            int index = 0;
            string spellUp = "";
            
            if (m_HitSpellsPresent[0] == 1 && GetSpellDown("ButtonA","ButtonLT"))
            {
                spellUp = "ButtonA";
                index = 0;
                spellFound = true;
            } else if (m_HitSpellsPresent[1] == 1 && GetSpellDown("ButtonB","ButtonLT"))
            {
                spellUp = "ButtonB";
                index = 1;
                spellFound = true;
            } else if (m_HitSpellsPresent[2] == 1 && GetSpellDown("ButtonY","ButtonLT"))
            {
                spellUp = "ButtonY";
                index = 2;
                spellFound = true;
            } else if (m_HitSpellsPresent[3] == 1 && GetSpellDown("ButtonX","ButtonLT"))
            {
                spellUp = "ButtonX";
                index = 3;
                spellFound = true;
            } else if (m_HealSpellsPresent[0] == 1 && GetSpellDown("ButtonA","ButtonRT"))
            {
                spellUp = "ButtonA";
                array = m_HealSpells;
                index = 0;
                spellFound = true;
            } else if (m_HealSpellsPresent[1] == 1 && GetSpellDown("ButtonB","ButtonRT"))
            {
                spellUp = "ButtonB";
                array = m_HealSpells;
                index = 1;
                spellFound = true;
            } else if (m_HealSpellsPresent[2] == 1 && GetSpellDown("ButtonY","ButtonRT"))
            {
                spellUp = "ButtonY";
                array = m_HealSpells;
                index = 2;
                spellFound = true;
            } else if (m_HealSpellsPresent[3] == 1 && GetSpellDown("ButtonX","ButtonRT"))
            {
                spellUp = "ButtonX";
                array = m_HealSpells;
                index = 3;
                spellFound = true;
            }

            if (!spellFound) return;
            
            m_HasLaunchedSpell = true;
            StartCoroutine(WaitForUpCoroutine(
                new LaunchSpell()
                {
                    eEntity = this, eSpell = array[index], hitArray = array.Equals(m_HitSpells), spellArrayIndex = index
                }, spellUp));
                       
        }

        private IEnumerator WaitForUpCoroutine([CanBeNull] LaunchSpell e, string spellUp)
        {
            // TODO: DIsplay cases
            
            for (;;)
            {
                if (GetSpellUp(spellUp))
                    break;
                yield return null;
            }
            EventManager.Instance.Raise(e);
            yield return null;
        }

        private bool GetSpellDown(string button1, string button2)
        {
            return Input.GetButtonDown(button1) && (Input.GetAxis(button2) > 0 || Input.GetButton(button2));
        }

        private bool GetSpellUp(string button)
        {
            if (Input.GetButtonUp(button))
                Debug.Log("button up");
            return Input.GetButtonUp(button);
        }

        private void AutomaticSpellCheck()
        {
            if ((float)m_HealthPoints / m_maxHealth < 0.5)
            {
                if (!GetFirstHealSpell())
                    GetFirstHitSpell();
            }
            else
            {
                if (!GetFirstHitSpell() && m_HealthPoints < m_maxHealth)
                    GetFirstHealSpell();
            }
        }

        private bool GetFirstHealSpell()
        {
            for (int i = 0; i < m_HealSpells.Length; i++)
            {
                if (m_HealSpellsPresent[i] <= 0) continue;
                m_HasLaunchedSpell = true;
                EventManager.Instance.Raise(new LaunchSpell() {eEntity = this, eSpell = m_HealSpells[i], hitArray = false, spellArrayIndex = i});
                return true;
            }

            return false;
        }

        private bool GetFirstHitSpell()
        {
            for (int i = 0; i < m_HitSpells.Length; i++)
            {
                if (m_HitSpellsPresent[i] <= 0) continue;
                m_HasLaunchedSpell = true;
                EventManager.Instance.Raise(new LaunchSpell() {eEntity = this, eSpell = m_HitSpells[i], hitArray = true, spellArrayIndex = i});
                return true;
            }

            return false;
        }

        #endregion


        #endregion

        #region Entity functions
        

        #endregion
    }

    public enum Team
    {
        Blue,
        Red,
        Neutral
    }

    
}